import { User } from 'src/app/models/user.model';
import { UserRole } from 'src/app/models/userRole.model';
// import { UserRole } from "./../models/userRole.model";
// import { User } from "./../models/user.model";
import { Eligible } from "src/app/models/eligible.model";
// import { Stats } from "./../models/stats.model";
import { Stats } from 'src/app/models/stats.model'
import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import TopicTree from "../models/TopicTree.model";
import { AuthService } from "./auth.service";
import TopicRequest from "../models/TopicRequest.model";
import Profile, { ProfileRequest } from "../models/Profile.model";
import { environment } from "src/environments/environment.prod";
import { query } from '@angular/animations';

const httpOptions = {
	headers: new HttpHeaders({
		"Content-Type": "application/json"
	})
};

@Injectable({
  	providedIn: "root"
})

export class MainService {

  private baseUrl: string = environment.URL;
	private URL: string = environment.URL;

	private topicTreeAPI: string = `${this.URL}/topictree`;
	private profileAPI: string = `${this.URL}/profile`;
	private topicRequestAPI: string = `${this.URL}/topicrequest`;
	private getProfileByUserIDAPI: string = `${this.URL}/user/${this.auth.getUserInfo()?.id}/profile`;

	private handleError({ error, status }: HttpErrorResponse): Observable<never> {
		if (!status) this.toastr.error("Connection Error");
		else this.toastr.error(error?.message || "Something Went Wrong");
		return throwError(error);
	}

	constructor(private http: HttpClient, private toastr: ToastrService, private auth: AuthService) { }

	// Topic Tree
	getTopicTree(): Observable<{ data: Object }> {
		return this.http
			.get<{ data: Object }>(this.topicTreeAPI, httpOptions)
	}

	addTopicTree(topicTree: TopicTree): Observable<{ data: Object }> {
		return this.http
			.post<{ data: Object }>(this.topicTreeAPI, topicTree, httpOptions)
	}

	editTopicTree(topicTree: TopicTree, id: number): Observable<{ data: Object }> {
		return this.http
			.put<{ data: Object }>(`${this.topicTreeAPI}/${id}`, topicTree, httpOptions)
	}

	deleteTopicTree(id: number): Observable<string> {
		return this.http
			.delete<string>(`${this.topicTreeAPI}/${id}`, httpOptions)
	}

	// Profile
	getProfileByUserID(): Observable<{ data: Object }> {
		return this.http
			.get<{ data: Object }>(this.getProfileByUserIDAPI, httpOptions)
	}

	getProfileByID(id: number): Observable<{ data: Profile }> {
		return this.http
			.get<{ data: Profile }>(`${this.profileAPI}/${id}`, httpOptions)
	}

	addProfile(profile: ProfileRequest): Observable<{ data: Object }> {
		return this.http
			.post<{ data: Object }>(this.profileAPI, profile, httpOptions)
	}

	editProfile(profile: ProfileRequest, id: number): Observable<{ data: Object }> {
		return this.http
			.put<{ data: Object }>(`${this.profileAPI}/${id}`, profile, httpOptions)
	}

	deleteProfile(id: number): Observable<string> {
		return this.http
			.delete<string>(`${this.profileAPI}/${id}`, httpOptions)
	}

	// Topic Request
	getTopicRequests = (): Observable<{ data: {} }> => this.http
		.get<{ data: {} }>(this.topicRequestAPI, httpOptions)

	addTopicRequest = (topicRequest: TopicRequest): Observable<{ data: {} }> => this.http
		.post<{ data: {} }>(this.topicRequestAPI, topicRequest, httpOptions)

	deleteTopicRequest = (id: number): Observable<string> => this.http
		.delete<string>(`${this.topicRequestAPI}/${id}`, httpOptions)

	getAllPrivileges(){
		return this.http.get(this.baseUrl + "/privilege")
	}

	getAllRoles()
	{
		return this.http.get(this.baseUrl + "/userroles")
	}

	addUserRole(userRole: UserRole){
		return this.http.post(this.baseUrl + "/userroles", userRole)
	}

	editUserRole(userRole: UserRole, id: number){
		return this.http.put(this.baseUrl + "/userroles/" + id, userRole)
	}

	deleteUserRole(id: number){
		return this.http.delete(this.baseUrl + "/userroles/" + id)
	}

	//for assign role
	getUserRoleById(id: number){
		return this.http.get(this.baseUrl + "/userroles/" + id)
	}

	getUsers(){
		return this.http.get(this.baseUrl + "/user")
	}

  EditUser(user_id: string, body: any) {
    return this.http.put(`${this.baseUrl}/user/${user_id}`, body);
  }

	assignRole(roles_and_users: any){
		return this.http.post(this.baseUrl + "/user", roles_and_users)
	}

	//for validate and index
	validateDocuments(toBeValidated: any): Observable<string>{
		return this.http.post<string>(this.baseUrl + "/validatedocuments", toBeValidated)
	}

	indexDocuments(toBeIndexed: any): Observable<string>{
		return this.http.post<string>(this.baseUrl + "/indexdocuments", toBeIndexed)
	}

	getAllDocuments(): Observable< Object[] >{
		return this.http.get<{ data: any[] }>(this.baseUrl + "/document", httpOptions)
				.pipe(map(response => response.data || []))
	}

  getValidatedDocuments() {
    return this.http.get(`${this.baseUrl}/document?status=2`)
  }

  getVisibleDocuments() {
    return this.http.get(`${this.baseUrl}/document?status=1`)
  }

	getDocumentByID(document_id: number){
		return this.http.get<{ data: any[] }>(this.baseUrl + "/document/" + document_id)
    .pipe(map(response => response.data || []))
	}

	getDocumentByUser(user_id: number){
		return this.http.get<{ data: any[] }>(this.baseUrl + "/user/" + user_id + "/document")
    .pipe(map(response => response.data || []))
	}

	hideDocument(document_id: number): Observable<string>{
		return this.http.get<string>(this.baseUrl + "/hidedocument/" + document_id)
	}

	postDocument(documentToBePosted: any){
		return this.http.post<{ data: any }>(this.baseUrl + "/document", documentToBePosted)
        .pipe(map(res => res.data || []))
	}

	deleteDocument(document_id: number): Observable<string>{
		return this.http.delete<string>(this.baseUrl + "/document/" + document_id)
	}

	editDocument(document_id: number, documentToBeEdited: any){
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };
		return this.http.put(this.baseUrl + "/document/" + document_id, documentToBeEdited.toString(), options)
	}

	getStats(): Observable<Stats>{
		return this.http.get<Stats>(this.baseUrl + "/stats")
	}

	getEligibleUsers(){
		return this.http.get(this.baseUrl + "/eligibleuser")
	}

	postEligibleUser(eligibleUserToBePosted: Eligible){
		return this.http.post(this.baseUrl + "/eligibleuser", eligibleUserToBePosted)
	}

	editAcount(editedAccount: User, user_id: string){
		return this.http.put(this.baseUrl + "/user/" + user_id, editedAccount)
	}

	resetPassword(data: any){
		return this.http.post(this.baseUrl + "/reset-password", data)
	}

  RetreiveDocuments(queryString: string) {
    return this.http.get<{ data: any[] }>(`${this.baseUrl}/retrieve-documents?queryString=${queryString}`)
      .pipe(map(res => res.data || []))
  }

  RelvenceFeedbackDocuments(documentString: string, queryString: string) {
    console.log(`${this.baseUrl}/relevance-feedback?documents=[${documentString}]&queryString=${queryString}`)
    return this.http.get<{ data: any[] }>(`${this.baseUrl}/relevance-feedback?documents=['d54', 'd53']&queryString=pdf`)
      .pipe(map(res => res.data || []))
  }


}
