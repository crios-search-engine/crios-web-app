import { Country } from 'src/app/models/country.model';
import { environment } from 'src/environments/environment.prod';
import { Injectable } from '@angular/core';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { HttpClient, JsonpClientBackend } from '@angular/common/http';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class SignedInService implements CanActivate {
  constructor(private router: Router) {}
  canActivate(): boolean {
    if (localStorage.getItem('user_data')) {
      this.router.navigate(['home']);
      return false;
    }
    return true;
  }
}

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  private BASE_URL: string = environment.URL;
  private ELIGIBILITY_URL: string = `${this.BASE_URL}/check-user`;
  private SIGN_UP_URL: string = `${this.BASE_URL}/signup`;
  private SIGN_IN_URL: string = `${this.BASE_URL}/signin`;
  private COUNTRY_URL: string = `${this.BASE_URL}/country`;
  private FORGOT_PASSWORD_URL: string = `${this.BASE_URL}/forgot-password`;
  private RESET_PASSWORD_URL: string = `${this.BASE_URL}/reset-password`;

  constructor(
    private router: Router,
    private socialAuthService: SocialAuthService,
    private httpClient: HttpClient,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (!localStorage.getItem('user_data')) {
      this.router.navigate(['signin']);
      return false;
    }
    return true;
  }

  canA;

  public resetPassword(data: any) {
    return this.httpClient.post(`${this.RESET_PASSWORD_URL}`, data).toPromise();
  }

  public forgotPassword(data: any) {
    return this.httpClient
      .post(`${this.FORGOT_PASSWORD_URL}`, data)
      .toPromise();
  }

  public checkEligibility(data: any) {
    return this.httpClient.get(`${this.ELIGIBILITY_URL}/${data}`).toPromise();
  }

  public signIn(data: object) {
    return this.httpClient.post(`${this.SIGN_IN_URL}`, data).toPromise();
  }

  public signUp(data: object) {
    return this.httpClient.post(`${this.SIGN_UP_URL}`, data).toPromise();
  }

  public getCountry(){
    return this.httpClient.get(`${this.COUNTRY_URL}`).toPromise();
  }

  getLocalStorage()
  {
    return localStorage.getItem("user_data");
  }

  getUserInfo(): User
  {
    return JSON.parse(localStorage.getItem("user_data"))?.data;
  }

  isLogged() : boolean {
    return !!localStorage.getItem("user_data");
  }

  GetTokens() {
    return JSON.parse(localStorage.getItem("user_data"))?.token;
  }

  isRolePrivilge() {
    return this.getUserInfo()?.user_role?.privileges?.some(function(el) {
      return el.code === "001";
    });
  }

  isValidatePrivilege() {
    return this.getUserInfo()?.user_role?.privileges?.some(function(el) {
      return el.code === "002";
    });
  }

  isIndexPrivilege() {
    return this.getUserInfo()?.user_role?.privileges?.some(function(el) {
      return el.code === "003";
    });
  }

  isTopicTreePrivilege() {
    return this.getUserInfo()?.user_role?.privileges?.some(function(el) {
      return el.code === "004";
    });
  }

  isStatisticsPrivilege() {
    return this.getUserInfo()?.user_role?.privileges?.some(function(el) {
      return el.code === "005";
    });
  }

  isEligiblePrivilege() {
    return this.getUserInfo()?.user_role?.privileges?.some(function(el) {
      return el.code === "006";
    });
  }
}
