import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    // add auth header with jwt if account is logged in and request is to the api url
    const token = this.authService.GetTokens();
    const isLoggedIn = this.authService.isLogged();
    const isApiUrl = request.url.startsWith(environment.URL);

    if (isLoggedIn && isApiUrl) {
      request = request.clone({
          setHeaders: { Authorization: `Bearer ${token}` }
      });
  }

    return next.handle(request);
  }
}
