import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import {catchError} from "rxjs/operators";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(private toastr: ToastrService) {}

  private handleError({ error, status }: HttpErrorResponse): Observable<never> {
		if (!status) this.toastr.error("Connection Error");
		else this.toastr.error(error?.message || "Something Went Wrong");
		return throwError(error);
	}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
    .pipe(catchError(this.handleError.bind(this)));
  }
}
