import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user_id: string = "";

  constructor(
              private router: Router,
              private authService: AuthService
              ) { }

  ngOnInit() {
    this.user_id = JSON.parse(localStorage.getItem('user_data')).data.id;
  }
  
  open() {

    const overlay = document.getElementsByClassName('overlay') as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < overlay.length; i++) {
      overlay[i].style.display = 'block';

    }


  }

  signOut(){
    localStorage.removeItem('user_data');
    this.router.navigate([""])
  }

  hide() {
    const overlay = document.getElementsByClassName('overlay') as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < overlay.length; i++) {
      overlay[i].style.display = 'none';

    }
  }
}
