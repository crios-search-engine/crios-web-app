import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import TopicTree from "src/app/models/TopicTree.model";

@Component({
	selector: "app-add-topic",
	templateUrl: "./add-topic.component.html",
	styleUrls: ["./add-topic.component.css"]
})
export class AddTopicComponent implements OnInit {

	title: string;

	@Output() addTopic: EventEmitter<{ topicTree: TopicTree, callback: Function }> = new EventEmitter();

	@ViewChild("closeModalBtn") closeModalBtn: ElementRef;

	selectedTopic: string;

	@Input() topicTree: TopicTree[] = [];
	@Input() loading: boolean;
	parentTopics: TopicTree[] = [];

	constructor() { }

	ngOnInit() { }

	ngOnChanges() {
		this.parentTopics = [];
		this.topicTree.forEach(branch => this.flatten(branch));
	}

	flatten = (branch: TopicTree): void => {
		if (Array.isArray(branch.subtopics)) { 
			this.parentTopics.push(branch);
			branch.subtopics.forEach(branch => this.flatten(branch));
		} else 
			this.parentTopics.push(branch);
	}

	save(): void {
		this.addTopic.emit({  
			topicTree: { 
				title: this.title,
				status: 1,
				topicparent_id: this.selectedTopic === null ? null : parseInt(this.selectedTopic)
			},
			callback: () => {
				this.selectedTopic = "";
				this.title = "";
				this.closeModalBtn.nativeElement.click();
			}
		});
    }
}