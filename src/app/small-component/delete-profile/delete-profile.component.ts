import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { MainService } from "src/app/services/main.service";
import { ActiveToast, ToastrService } from "ngx-toastr";

@Component({
	selector: "app-delete-profile",
	templateUrl: "./delete-profile.component.html",
	styleUrls: ["./delete-profile.component.css"]
})

export class DeleteProfileComponent {

	@Output() getProfiles: EventEmitter<void> = new EventEmitter();
	@Output() setLoading: EventEmitter<boolean> = new EventEmitter();

	@Input() id: number;
	@Input() loading: boolean;

	@ViewChild("closeModalBtn") closeModalBtn: ElementRef;

	constructor(private API: MainService, private toastr: ToastrService) { }

	deleteProfile(): void|ActiveToast<ToastrService> {
		if (!this.id) return this.toastr.error("Something Went Wrong...");
		this.setLoading.emit(true);
		this.API.deleteProfile(this.id).subscribe(res => {
			this.toastr.success(res || "Successfully Deleted Profile");
			this.closeModalBtn.nativeElement.click();
			this.getProfiles.emit();
		}, err => {
			console.error(err);
			this.setLoading.emit(false);
		});
	}

}