import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-delete-role',
  templateUrl: './delete-role.component.html',
  styleUrls: ['./delete-role.component.css']
})
export class DeleteRoleComponent implements OnInit {

  @Output() deleteRole: EventEmitter<Function> = new EventEmitter();
  @ViewChild("closeModalBtn") closeModalBtn: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  deleteRoleId() {
		this.deleteRole.emit(() => this.closeModal());
	}

  closeModal(): void {
		this.closeModalBtn.nativeElement.click();
	}
}
