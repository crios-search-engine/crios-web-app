import { Component, OnInit, Output, Input, ViewChild, EventEmitter, ElementRef } from "@angular/core";
import TopicTree from "src/app/models/TopicTree.model";

@Component({
	selector: "app-add-sub-topic",
	templateUrl: "./add-sub-topic.component.html",
	styleUrls: ["./add-sub-topic.component.css"]
})

export class AddSubTopicComponent implements OnInit {

	title: string;

	@Output() addSubTopic: EventEmitter<{ title: string, callback: Function }> = new EventEmitter();

	@ViewChild("closeModalBtn") closeModalBtn: ElementRef;

	@Input() loading: boolean;

	constructor() { }

	ngOnInit() { }

	save(): void {
		this.addSubTopic.emit({  
			title: this.title,
			callback: () => {
				this.title = "";
				this.closeModalBtn.nativeElement.click();
			}
		});
    }

}