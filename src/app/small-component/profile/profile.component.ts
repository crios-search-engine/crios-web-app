import { Component, Input, Output, EventEmitter } from "@angular/core";
import Profile from "../../models/Profile.model";

@Component({
	selector: "app-profile",
	templateUrl: "./profile.component.html",
	styleUrls: ["./profile.component.css"]
})

export class ProfileComponent {

	@Output() getProfiles: EventEmitter<void> = new EventEmitter();
	@Output() setLoading: EventEmitter<boolean> = new EventEmitter();

  @Input() profile: Profile;
	@Input() loading: boolean;

	constructor() { }

	updateProfiles(): void {
		this.getProfiles.emit();
	}

	updateLoading(loading: boolean) {
		this.setLoading.emit(loading);
	}
}
