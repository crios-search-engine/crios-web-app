import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-delete-document',
  templateUrl: './delete-document.component.html',
  styleUrls: ['./delete-document.component.css']
})
export class DeleteDocumentComponent implements OnInit {

  @ViewChild("closeModalBtn") closeModalBtn: ElementRef;
  @Output() deleteDocument = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  deleteDoc() {
    this.deleteDocument.emit(() => this.closeModal());
  }

  closeModal() {
    this.closeModalBtn.nativeElement.click();
  }
}
