import {Component, Input, OnInit} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-view-document',
  templateUrl: './view-document.component.html',
  styleUrls: ['./view-document.component.css']
})
export class ViewDocumentComponent implements OnInit {

  headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');

  @Input() documentUrl: string;

  constructor() {

  }

  ngOnInit(): void {
  }

}
