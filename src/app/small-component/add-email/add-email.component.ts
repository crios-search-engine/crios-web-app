import { MainService } from 'src/app/services/main.service';
import { Eligible } from 'src/app/models/eligible.model';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-email',
  templateUrl: './add-email.component.html',
  styleUrls: ['./add-email.component.css']
})
export class AddEmailComponent implements OnInit {
  
  @Output() refreshEligible = new EventEmitter();
  
  eligible: Eligible = {
    username: "",
    email: ""
  }
  
  constructor(
              private mainService: MainService,
              private toastr: ToastrService,
            ) { }

  ngOnInit(): void {
  }

  submit(){
    if(this.eligible.email !== "" && this.eligible.username !== ""){
      if(this.ValidateEmail(this.eligible.email)){
        console.log(this.eligible);
        this.addEligible(this.eligible);
        this.eligible = {
          username: "",
          email: ""
        }
      }
    }
  }
  
  addEligible(elig: Eligible){
    this.mainService.postEligibleUser(elig).subscribe(
      res => {
        this.toastr.success("Successfully Added Email");
        this.refreshEligible.emit();
      }
    );
  }

  ValidateEmail(mail) 
  {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
    {
      return (true)
    }
      // alert("You have entered an invalid email address!")
      this.toastr.error("Invalid Email Address!");
      return (false)
  }

}
