import { Component, OnInit, Input, Output, ViewChild, EventEmitter, ElementRef } from "@angular/core";
import TopicTree from "src/app/models/TopicTree.model";

@Component({
	selector: "app-edit-topic",
	templateUrl: "./edit-topic.component.html",
	styleUrls: ["./edit-topic.component.css"]
})

export class EditTopicComponent implements OnInit {

	@Output() editTopic: EventEmitter<{ topicTree: TopicTree, callback: Function }> = new EventEmitter();
	@Output() cancelEditTopicRequest: EventEmitter<void> = new EventEmitter();

	@ViewChild("closeModalBtn") closeModalBtn: ElementRef;

	selectedTopic: number|null|string;
	title: string;
	parentTopics: TopicTree[] = [];

	@Input() loading: boolean;
	@Input() topicTree: TopicTree[] = [];
	@Input() selectedParentID: number|null;
	@Input() selectedNodeID: number;
	@Input() selectedTitle: string;

	constructor() { }

	ngOnInit() {
		this.topicTree.forEach(branch => this.flatten(branch));
	}

	ngOnChanges() {
		if (!this.loading) {
			this.title = this.selectedTitle;
			this.selectedTopic = this.selectedParentID;
		}
		this.parentTopics = [];
		this.topicTree.forEach(branch => this.flatten(branch));
	}

	flatten = (branch: TopicTree): void => {
		if (Array.isArray(branch.subtopics)) { 
			// First condition is for not showing the same topic that's chosen
			// Second condition is for not showing child topics within the same parent
			if (branch.id !== this.selectedNodeID && branch.topicparent_id !== this.selectedNodeID) {
				this.parentTopics.push(branch);
				branch.subtopics.forEach(branch => this.flatten(branch));
			}
		} else {
			if (branch.id !== this.selectedNodeID && branch.topicparent_id !== this.selectedNodeID)
				this.parentTopics.push(branch);
		}
	}

	cancelEditModal(e?: Event|undefined): void {
		if (e) e.stopPropagation();
		this.cancelEditTopicRequest.emit();
	}

	stopPropagation(e: Event): void {
		e.stopPropagation();
	}

	save(): void {
		this.editTopic.emit({  
			topicTree: { 
				title: this.title,
				topicparent_id: this.selectedTopic === null ? null : parseInt(this.selectedTopic.toString())
			},
			callback: () => {
				this.selectedTopic = "";
				this.title = "";
				this.closeModalBtn.nativeElement.click();
			}
		});
	}
}