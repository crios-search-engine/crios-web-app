import { ActiveToast, ToastrService } from 'ngx-toastr';
import { UserRole } from './../../models/userRole.model';
import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewRoleComponent } from 'src/app/pages/roles/view-role/view-role.component';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  @Input() role: UserRole;
  @Output() selectedRoleId: EventEmitter<number> = new EventEmitter();

  constructor(
              private mainService: MainService,
              private modalService: NgbModal,
              private viewComponent: ViewRoleComponent,
              private toastr: ToastrService,
              ) {
  }

  ngOnInit() {
    
  }

 
  setRoleId(roleId): void{
    this.selectedRoleId.emit(roleId);
    // console.log(this.selectedRoleId);
  }

  // deleteRole = (callback: Function): void|ActiveToast<ToastrService> =>{
    
  //   if(!this.selectedRoleId){
  //     console.log(this.selectedRoleId);
  //     return this.toastr.error("Something Went Wrong...");
  //   }
  //   console.log(this.selectedRoleId);
  //   this.mainService.deleteUserRole(this.selectedRoleId).subscribe(
  //     res => {
  //       this.viewComponent.getRoles();
  //       this.toastr.success("Successfully Deleted a Role");
  //       callback();
  //     },
  //     err => {
  //       console.log(err);
        
  //     }
  //   );
  // }

  
  openModal(content: TemplateRef<any>, userRoleId: any){
    this.selectedRoleId = userRoleId;
    this.modalService.open(content, { backdrop: 'static', centered: true , size: 'xl' as 'lg'});
  }

}