import { MainService } from 'src/app/services/main.service';
import { AuthService } from './../../services/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  isEqualPassword: boolean = false;
  form: FormGroup;
  password = '';
  repassword = '';
  user_data = JSON.parse(this.authService.getLocalStorage());
  @ViewChild("closeModalBtn") closeModalBtn: ElementRef;

  constructor(
              private authService: AuthService,
              private mainService: MainService
              ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      oldPassword: new FormControl('', [Validators.required]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.minLength(8)])
    })
  }

  checkMatchValidator(field1: string, field2: string) {
    this.isEqualPassword = field1 == field2;
  }

  submit(){
    let data = {
      token: this.user_data?.token,
      email: this.user_data?.data.email,
      password: this.form.get("newPassword").value,
      password_confirmation: this.form.get("confirmPassword").value,
    }
    console.log(data);
    this.mainService.resetPassword(data).subscribe(
      res => {
        if(res){
          this.closeModalBtn.nativeElement.click();
          console.log(res)
        }
      }
    );
  }

}
