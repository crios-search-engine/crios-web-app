import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { MainService } from "src/app/services/main.service";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "src/app/services/auth.service";

@Component({
	selector: "app-request-new-topic",
	templateUrl: "./request-new-topic.component.html",
	styleUrls: ["./request-new-topic.component.css"]
})

export class RequestNewTopicComponent {

	topicDescription: string;

	@Output() setLoading: EventEmitter<boolean> = new EventEmitter();

	@Input() loading: boolean;

	@ViewChild("closeModalBtn") closeModalBtn: ElementRef;

	constructor(private API: MainService, private toastr: ToastrService, private auth: AuthService) { }

	save(): void { 
		this.updateLoading(true);
		this.API.addTopicRequest({  
			description: this.topicDescription,
			user_id: this.auth.getUserInfo()?.id
		}).subscribe(() => {
			this.toastr.success("Successfully Added a Topic Tree");
			this.topicDescription = "";
			this.closeModalBtn.nativeElement.click();
		}, err => console.error(err), () => this.updateLoading(false));
	}

	updateLoading(loading: boolean): void {
		this.setLoading.emit(loading);
	}

}