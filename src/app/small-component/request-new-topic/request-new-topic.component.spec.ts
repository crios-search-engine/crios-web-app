import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestNewTopicComponent } from './request-new-topic.component';

describe('RequestNewTopicComponent', () => {
  let component: RequestNewTopicComponent;
  let fixture: ComponentFixture<RequestNewTopicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestNewTopicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestNewTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
