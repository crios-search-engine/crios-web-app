import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-choose-profile',
  templateUrl: './choose-profile.component.html',
  styleUrls: ['./choose-profile.component.css']
})
export class ChooseProfileComponent implements OnInit {

  constructor() { }
  topics = ['topic 1', 'topic 2', 'topic 3', 'topic 4'];

  ngOnInit(): void {
  }

}
