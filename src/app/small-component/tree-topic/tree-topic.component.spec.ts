import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeTopicComponent } from './tree-topic.component';

describe('TreeTopicComponent', () => {
  let component: TreeTopicComponent;
  let fixture: ComponentFixture<TreeTopicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreeTopicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
