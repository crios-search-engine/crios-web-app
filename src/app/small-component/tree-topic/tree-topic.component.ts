import { MainService } from 'src/app/services/main.service';
import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import TopicTree, { TopicTreeFlatNode } from 'src/app/models/TopicTree.model';
import TopicRequest from 'src/app/models/TopicRequest.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tree-topic',
  templateUrl: './tree-topic.component.html',
  styleUrls: ['./tree-topic.component.css']
})
export class TreeTopicComponent implements OnInit, OnChanges {

  @Input() topicTree?: TopicTree[] = [];

  isAnEditTopicRequest: boolean;

	loading: boolean = true;
	topicRequestLoading: boolean;
	searchInProgress: boolean;

	search: string;
	selectedNodeID: number;
	selectedTitle: string;
	selectedParentID: number|null;

	filteredTree: TopicTree[] = [];
	topicRequests: TopicRequest[] = [];
	removedBranch: TopicTree;

	filteredTopicTree:  any[] = [];

  	treeControl = new FlatTreeControl<TopicTreeFlatNode>(({ level }) => level, ({ expandable }) => expandable);

  	treeFlattener = new MatTreeFlattener(
		({ id, topicparent_id, subtopics, title }: TopicTree, level: number) => ({
			expandable: subtopics?.length > 0,
			id,
			topicparent_id,
			title,
			level
		}),
		({ level }) => level,
		({ expandable }) => expandable,
		({ subtopics }) => subtopics
	);

  	dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener) || null;

	constructor(private mainService: MainService, private toastr: ToastrService) {
		// this.dataSource.data = this.topicTree;

		// this.getTopicTree();
	}

	ngOnInit(): void {

	}

	ngOnChanges(){
		// this.dataSource.data = this.topicTree;
		// this.dataSource.data = [];
		// console.log(this.topicTree);

		this.getTopicTree();
	}

	filterBranch = (branch: TopicTree, search: string): void => {
		if (Array.isArray(branch.subtopics)) {
			if (branch.title.toLowerCase().includes(search))
				this.filteredTree.push(branch);
			else branch.subtopics.forEach(branch => this.filterBranch(branch, search));
		} else {
			if (branch.title.toLowerCase().includes(search))
				this.filteredTree.push(branch);
		}
	}
	searchAndAddBranch = (leaf: TopicTree, branch: TopicTree): TopicTree => {
		if (this.searchInProgress) {
			if (Array.isArray(leaf.subtopics)) {
				if (branch.topicparent_id === leaf.id) {
					leaf.subtopics = [
						...[...leaf.subtopics || []],
						branch
					];
					this.searchInProgress = false;
				} else leaf.subtopics.forEach(leaf => this.searchAndAddBranch(leaf, branch));
			} else {
				if (branch.topicparent_id === leaf.id) {
					leaf.subtopics = [
						...[...leaf.subtopics || []],
						branch
					];
					this.searchInProgress = false;
				}
			}
		}
		return leaf;
	}


	hasChild = (_: number, node: TopicTreeFlatNode) => node.expandable;
	isParent = (node: TopicTreeFlatNode) => node.level === 0;

	searchTree = (): void => {
		this.filteredTree = [];
		this.topicTree.forEach(branch => this.filterBranch(branch, this.search.toLowerCase()));
		this.dataSource.data = this.filteredTree;
	}

	onSearchChange = (): void => {
		if (!this.search)
			this.dataSource.data = this.topicTree;
	}

	addTopic = (branch: TopicTree): void => {
		if (!branch.topicparent_id) // if it's a parent branch
			this.topicTree.unshift(branch);
		else {
			this.searchInProgress = true;
			this.topicTree = this.topicTree.map(leaf => this.searchAndAddBranch(leaf, branch));
		}
	}
	addTopics = (topics: TopicTree[]): void => {
		this.topicTree = [];
		if (topics?.length > 0) topics.forEach(topic => this.addTopic(topic));
		this.dataSource.data = this.topicTree;

		//remove duplicates objects from subtopics
		this.dataSource.data.forEach((data, index) =>{
			const arrUniq = [...new Map(data?.subtopics?.map(v => [v.id, v])).values()]
			this.topicTree[index].subtopics = arrUniq;
			console.log(this.topicTree[index].subtopics);
		})

		this.dataSource.data = this.topicTree;

	}

	selectNodeID(id: number): void {
		this.selectedNodeID = id;
	}

	selectNode(id: number, title: string, topicparent_id: number|null = null): void {
		this.selectedNodeID = id;
		this.selectedTitle = title;
		this.selectedParentID = topicparent_id;
	}

	getEditRequestData(id: number, description: string): void {
		this.selectedNodeID = id;
		this.selectedTitle = description;
		this.selectedParentID = null;
		this.isAnEditTopicRequest = true;
	}

	cancelEditTopicRequest(): void {
		this.isAnEditTopicRequest = false;
	}

	// API Functions
  getTopicTree(): void {
    this.addTopics(<TopicTree[]> this.topicTree);
}
}
