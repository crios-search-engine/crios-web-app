import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter } from "@angular/core";

@Component({
	selector: "app-delete-topic",
	templateUrl: "./delete-topic.component.html",
	styleUrls: ["./delete-topic.component.css"]
})

export class DeleteTopicComponent implements OnInit {

	@Output() deleteTopic: EventEmitter<Function> = new EventEmitter();

	@ViewChild("closeModalBtn") closeModalBtn: ElementRef;

	@Input() loading: boolean;

	constructor() { }

	ngOnInit() { }

	deleteBranch() {
		this.deleteTopic.emit(() => this.closeModal());
	}

	closeModal(): void {
		this.closeModalBtn.nativeElement.click();
	}

}