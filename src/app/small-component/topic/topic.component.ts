import {Component, OnInit} from '@angular/core';
import {TopicTreeComponent} from 'src/app/pages/topic-tree/topic-tree.component';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {
  constructor() {
  }

  clicked = false;


  ngOnInit() {
  }

  view($event: any) {
    if (!this.clicked) {
      // tslint:disable-next-line:max-line-length
      // document.querySelector("#sub-topics > div:nth-child(2) > div > div > div:nth-child(3) > app-topic > div > div > div:nth-child(3) > div:nth-child(1) > a")
      this.clicked = true;
    } else {
      this.clicked = false;
    }

  }


  delete() {

  }

  edit() {

  }
}
