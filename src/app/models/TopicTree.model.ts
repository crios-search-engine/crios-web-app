export default interface TopicTree {
    id?: number;
    title: string;
    status?: number;
    topicparent_id: number|null;
    subtopics?: TopicTree[]|null;
    created_at?: Date|string;
    updated_at?: Date|string;
}

export interface TopicTreeFlatNode {
    expandable: boolean;
    id: number;
    topicparent_id: number|null;
	title: string;
	level: number;
}