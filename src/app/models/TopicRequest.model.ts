export default interface TopicRequest {
    id?: number;
    description: string;
    user_id: string;
    user_firstname?: string;
    user_lastname?: string;
    created_at?: Date|string;
    updated_at?: Date|string;
}