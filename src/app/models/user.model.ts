import { Country } from './country.model';
import { UserRole } from './userRole.model';
export interface User{
    id?: string;
    email?: string;
    firstname: string;
    lastname: string;
    mobile: string;
    landline: string|null;
    address: string;
    biography?: string|null;
    status?: number;
    gender: number;
    birthdate: string;
    secondemail?: string|null;
    country: Country;
    user_role? : UserRole|null;
    created_at?: Date|string;
    updated_at?: Date|string;
}