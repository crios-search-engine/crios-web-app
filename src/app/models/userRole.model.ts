import { Privilege } from './privilege.model';
export interface UserRole{
    id?: string;
    title: string;
    privileges?: Privilege[];
    created_at?: Date|string;
    updated_at?: Date|string;
}