export interface Privilege {
    id: number;
    user_role_id: number;
    privilege_id: number;
    title: string;
    description: string;
    code: string;
    created_at?: Date|string;
    updated_at?: Date|string;
}