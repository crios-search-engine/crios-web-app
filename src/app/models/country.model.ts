export interface Country{
    id?: number;
    code?: string;
    name?: string;
    nationality?: string;
    phonecode?: string;
    created_at?: Date|string;
    updated_at?: Date|string;
}