import { User } from "./user.model";

export default interface Profile {
    id: number;
    title: string;
    description: string|null;
    user?: Array<User>;
    topics: ProfileTopicTree[];
    created_at?: Date|string;
    updated_at?: Date|string;
}

export interface ProfileTopicTree {
    id: number;
    profile_id: number;
    topictree_id: number;
    profiletitle: string;
    description: string;
    status: number;
    topictreetitle: string;
    created_at?: Date|string;
    updated_at?: Date|string;
}

export interface ProfileTopicTreeWithCheck {
    id?: number;
    title: string;
    status?: number;
    topicparent_id: number|null;
    subtopics?: ProfileTopicTreeWithCheck[]|null;
    created_at?: Date|string;
    updated_at?: Date|string;
    checked: boolean;
}

export interface ProfileTopicTreeFlatNode {
    expandable: boolean;
    id: number;
    topicparent_id: number|null;
    checked: boolean|false;
	title: string;
	level: number;
}

export interface ProfileRequest {
    title: string;
    description: string;
    user_id: string;
    topictree: Array<{ topictree_id: string }>
}