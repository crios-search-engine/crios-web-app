import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";

import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SignupComponent} from './pages/signup/signup.component';
import {SigninComponent} from './pages/signin/signin.component';
import {ForgotPasswordComponent} from './pages/forgot-password/forgot-password.component';
import {WelcomeComponent} from './pages/welcome/welcome.component';
import {HomeComponent} from './pages/home/home.component';
import {TopicTreeComponent} from './pages/topic-tree/topic-tree.component';
import {AddRoleComponent} from './pages/roles/add-role/add-role.component';
import {EditRoleComponent} from './pages/roles/edit-role/edit-role.component';
import {ViewRoleComponent} from './pages/roles/view-role/view-role.component';
import {AssignRoleComponent} from './pages/roles/assign-role/assign-role.component';
import {ViewProfilesComponent} from './pages/profiles/view-profiles/view-profiles.component';
import {AddProfileComponent} from './pages/profiles/add-profile/add-profile.component';
import {EditProfileComponent} from './pages/profiles/edit-profile/edit-profile.component';
import {IndexComponent} from './pages/index/index.component';
import {ValidateComponent} from './pages/validate/validate.component';
import {DocumentsComponent} from './pages/documents/documents/documents.component';
import {DocumentsAllComponent} from './pages/documents/documents-all/documents-all.component';
import {DocumentsMineComponent} from './pages/documents/documents-mine/documents-mine.component';
import {AddDocumentComponent} from './pages/documents/add-document/add-document.component';
import {EditDocumentComponent} from './pages/documents/edit-document/edit-document.component';
import {ViewAccountComponent} from './pages/accounts/view-account/view-account.component';
import {EditAccountComponent} from './pages/accounts/edit-account/edit-account.component';
import {EligibleComponent} from './pages/accounts/eligible/eligible.component';
import {FooterComponent} from "./footer/footer.component";
import {HeaderComponent} from "./header/header.component";
//////
import {MatInputModule} from '@angular/material/input';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
//////
//small component
import {TopicComponent} from './small-component/topic/topic.component';

import {DocumentComponent} from './small-component/document/document.component';

import {AddTopicComponent} from './small-component/add-topic/add-topic.component';

import {EditTopicComponent} from './small-component/edit-topic/edit-topic.component';

import {DeleteTopicComponent} from './small-component/delete-topic/delete-topic.component';

import {AddSubTopicComponent} from './small-component/add-sub-topic/add-sub-topic.component';

import {RoleComponent} from './small-component/role/role.component';

import {DeleteRoleComponent} from './small-component/delete-role/delete-role.component';

import { ProfileComponent } from './small-component/profile/profile.component';
import {ViewDocumentComponent} from "./small-component/view-document/view-document.component";
import{TreeTopicComponent} from "./small-component/tree-topic/tree-topic.component";
import{MatTabsModule} from "@angular/material/tabs";
import {DeleteDocumentComponent} from "./small-component/delete-document/delete-document.component";
import {ChooseProfileComponent} from "./small-component/choose-profile/choose-profile.component";
import {RequestNewTopicComponent} from "./small-component/request-new-topic/request-new-topic.component";
import {ChangePasswordComponent} from "./small-component/change-password/change-password.component";
import {AddEmailComponent} from "./small-component/add-email/add-email.component";
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ViewUserInfoComponent } from './small-component/view-user-info/view-user-info.component';
import { GoogleLoginProvider, SocialLoginModule } from 'angularx-social-login';
import { ToastrModule } from "ngx-toastr";
import { AuthService, SignedInService } from 'src/app/services/auth.service';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { VerifiedAccountComponent } from './pages/verified-account/verified-account.component';
import { DeleteProfileComponent } from "./small-component/delete-profile/delete-profile.component";
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ErrorHandlerInterceptor } from './interceptors/error-handler.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SignupComponent,
    SigninComponent,
    ForgotPasswordComponent,
    WelcomeComponent,
    HomeComponent,
    TopicTreeComponent,
    AddRoleComponent,
    EditRoleComponent,
    ViewRoleComponent,
    AssignRoleComponent,
    ViewProfilesComponent,
    AddProfileComponent,
    EditProfileComponent,
    IndexComponent,
    ValidateComponent,
    DocumentsComponent,
    DocumentsAllComponent,
    DocumentsMineComponent,
    AddDocumentComponent,
    EditDocumentComponent,
    ViewAccountComponent,
    EditAccountComponent,
    EligibleComponent,
    ///// small component
    TopicComponent,
    DocumentComponent,
    AddTopicComponent,
    EditTopicComponent,
    DeleteTopicComponent,
    AddSubTopicComponent,
    RoleComponent,
    DeleteRoleComponent,
    ProfileComponent,
    ViewDocumentComponent,
    TreeTopicComponent,
    DeleteDocumentComponent,
    ChooseProfileComponent,
    RequestNewTopicComponent,
    ChangePasswordComponent,
    AddEmailComponent,
    ViewUserInfoComponent,
    ResetPasswordComponent,
    VerifiedAccountComponent,
    DeleteProfileComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatInputModule,
    AppRoutingModule,
    CommonModule,
    SocialLoginModule,
    MatTreeModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatTabsModule,
    MatChipsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      closeButton: true,
      progressBar: true
    })
  ],
  providers: [
    {
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: true, //keeps the user signed in
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider('254135019807-9tkprk585lu83vseblte8rjk0h64dvmi.apps.googleusercontent.com') // your client id
        }
      ]
    }
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },

    AuthService,
    SignedInService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
