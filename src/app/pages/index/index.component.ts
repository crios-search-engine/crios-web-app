import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MainService } from 'src/app/services/main.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {

  passDocumentUrl: any = '';
  passTopicTree: any;
  checkedIndexDocs = [];
  indexedDocuments = [];
  loading = false;
  loadingIndex = false;
  PostIndexSubscription: Subscription = new Subscription();
  ValidatedDocsSubscription: Subscription = new Subscription();

  constructor(
    private modalService: NgbModal,
    private sanitizer: DomSanitizer,
    private mainService: MainService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.GetValidatedDocuments()
  }

  submit(){
    console.log({documents: this.checkedIndexDocs});
    if(this.checkedIndexDocs.length != 0){
      this.indexDocuments();
    }
  }

  indexDocuments() {
    this.loadingIndex = true;
    this.PostIndexSubscription = this.mainService.indexDocuments({documents: this.checkedIndexDocs}).subscribe(
      res => {
        this.GetValidatedDocuments();
        this.loadingIndex = false;
        this.toastr.success("Documents are indexed successfully");
        console.log(res);
      },
      err => {
        console.log(err);
        this.loadingIndex = false;
      },
      () => {
        this.loadingIndex = false;
      }
    );
  }

  displaySelectedConditions(event, doc_id: number) {
    if(event.target.checked) {
      let checked = this.checkedIndexDocs.find(Item => Item.document_id === doc_id);
      if(!checked){
        this.checkedIndexDocs.push({document_id: doc_id});
        console.log(this.checkedIndexDocs)
      }else{
        console.log("already exists");
      }
    }else{
      console.log("unchecked")
      let checked = this.checkedIndexDocs.find(Item => Item.document_id === doc_id);
      if(checked){
        this.checkedIndexDocs.forEach((element,index)=>{
          if(element.document_id == doc_id) this.checkedIndexDocs.splice(index,1);
        });
        console.log(this.checkedIndexDocs)
      }
    }
  }

  GetValidatedDocuments() {
    this.loading = true;
    this.ValidatedDocsSubscription = this.mainService.getValidatedDocuments()
    .subscribe(
      ( {data}: any ) :any => {
        this.indexedDocuments = data;
        this.loading = false;
        console.log("index doc", data)
        console.log("index doc", this.indexedDocuments.length)      },
      err => {
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openModalViewDocument(doc_url: any){
    this.passDocumentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(doc_url);
  }

  openModalViewTopicTree( tt: any){
    const modifiedTree = []
    tt.forEach(element => {
      console.log(element.topictree);
      modifiedTree.push(element.topictree[0])
    });

    this.passTopicTree = modifiedTree;
  }

  ngOnDestroy(): void {
    this.PostIndexSubscription.unsubscribe();
    this.ValidatedDocsSubscription.unsubscribe();
  }
}
