import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
})
export class ForgotPasswordComponent implements OnInit {
  form = new FormGroup({
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
  });
  constructor(private service: AuthService) {}
  emailSent: boolean = false;
  ngOnInit(): void {}

  submit() {
    console.log(this.form.valid);

    if (this.form.valid) {
      this.service
        .forgotPassword({ email: this.form.controls.emailFormControl.value })
        .then((res) => {
          this.emailSent = true;
        });
      // console.log(this.form.controls.emailFormControl.value);
    }
  }
}
