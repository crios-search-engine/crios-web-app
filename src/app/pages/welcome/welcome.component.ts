import { Stats } from './../../models/stats.model';
import { MainService } from 'src/app/services/main.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
})
export class WelcomeComponent implements OnInit {

  src_image = 'assets/homeIcon/Sign Up-In_Icon.png';

  stats: Stats;

  constructor(
              private service: AuthService,
              private router: Router,
              private mainService: MainService
              ) {}

  ngOnInit(): void {
    this.getStats();
    if (localStorage.getItem('user_data'))
      this.service
        .checkEligibility(JSON.parse(localStorage.getItem('user_data')).email)
        .then((res: any) => {
          console.log(res)
          if (res.registered) this.router.navigate(['/home']);
        });

    window.onscroll = function () {
      try {
        scrollFunction();
      } catch (e) {}
    };

    function scrollFunction() {
      if (
        document.body.scrollTop > 200 ||
        document.documentElement.scrollTop > 200
      ) {
        // @ts-ignore
        document.getElementById('navbar').style.position = 'fixed';
        // @ts-ignore
        document.getElementById('navbar').style.backgroundColor = 'white';
        // @ts-ignore
        document.getElementById('navbar').style.boxShadow = ' 0px 3px 3px grey';
        // @ts-ignore
        document
          .getElementById('icon')
          .setAttribute('src', 'assets/homeIcon/Icons_User.png');
        // @ts-ignore
        document.getElementById('logo').style.visibility = 'visible';
      } else {
        // @ts-ignore
        document.getElementById('navbar').style.position = 'absolute';
        // @ts-ignore
        document.getElementById('navbar').style.backgroundColor = 'transparent';
        // @ts-ignore
        document.getElementById('navbar').style.boxShadow = 'none';
        // @ts-ignore
        document
          .getElementById('icon')
          .setAttribute('src', 'assets/homeIcon/Sign Up-In_Icon.png');
        // @ts-ignore
        document.getElementById('logo').style.visibility = 'hidden';
      }
    }
  }

  open() {
    const overlay = document.getElementsByClassName(
      'overlay'
    ) as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < overlay.length; i++) {
      overlay[i].style.visibility = 'visible';
    }
  }

  hide() {
    const overlay = document.getElementsByClassName(
      'overlay'
    ) as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < overlay.length; i++) {
      overlay[i].style.visibility = 'hidden';
    }
  }

  getStats(){
    this.mainService.getStats().subscribe(
      (res: Stats) => {
        this.stats = res[0];
      }
    );
  }
}
