import { forkJoin, Subscription } from 'rxjs';
// import { UserRole } from './../../../models/userRole.model';
import { UserRole } from 'src/app/models/userRole.model';
// import { User } from './../../../models/user.model';
import { User } from 'src/app/models/user.model';
import { MainService } from 'src/app/services/main.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-assign-role',
  templateUrl: './assign-role.component.html',
  styleUrls: ['./assign-role.component.css']
})
export class AssignRoleComponent implements OnInit, OnDestroy {

  roles : UserRole[] = [];
  loading: boolean = false;
  users: User[] = [];
  roles_and_users = [];
  checkedUserRoles = [];
  form: FormGroup;
  UsersSubscription: Subscription = new Subscription();;
  RolesSubscription: Subscription = new Subscription();;

  constructor(
              private mainService: MainService,
              private toastr: ToastrService,
             ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      selectedRole: new FormControl(''),
    });

    this.getRoles();
    this.getUsers();
  }

  //get the roles to display them in select
  getRoles(){
    this.loading = true;
    this.RolesSubscription = this.mainService.getAllRoles().subscribe(
      (res : any) =>{
        this.roles = res.data
        this.loading = false;
      },
      err =>{
        console.log(err);
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  //get the all users
  getUsers(){
    this.loading = true;
    this.UsersSubscription = this.mainService.getUsers().subscribe(
      (res:any) => {
        this.users = res.data;
        this.roles_and_users = []
        this.users.forEach(element => {
          if(element.user_role){
            this.roles_and_users.push({user_role_id: element.user_role.id, user_id: element.id});
          }
         });
         this.loading = false;
      },
      err => {
        console.log(err);
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  submit(){
    this.assignRole(this.checkedUserRoles);
  }

  //set the roles_id and users_id in the array
  setRolesAndUsers(selectedValue, userId){
    let userRoleId = this.findSelectedValueID(selectedValue);

    if(userRoleId && userRoleId != ""){
      this.checkedUserRoles.forEach((element,index)=>{
        if(element.user_id == userId) this.checkedUserRoles.splice(index,1);
      });
      this.checkedUserRoles.push({user_role_id : userRoleId, user_id: userId});
    }else{
      this.checkedUserRoles.forEach((element,index)=>{
        if(element.user_id==userId) this.checkedUserRoles.splice(index,1);
      });
    }
  }

  assignRole(roles_and_users: any){

    this.loading = true;

    const reqArr = [];

    roles_and_users.forEach(element => {
      const { user_id, user_role_id } = element;
      reqArr.push(this.mainService.EditUser(user_id, {user_role_id}))
    });

    forkJoin(reqArr)
    .subscribe(
      res => {
        this.getUsers();
        this.toastr.info("Successfully Assigned!");
        this.loading = false;
      },
      err => {
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  isSelected(userRoleId: any, roleId: any): boolean {
    return this.roles_and_users.some(e => e.user_role_id === roleId && e.user_id === userRoleId);
  }

 findSelectedValueID(selectedValue: any){
    if(selectedValue)
      return this.roles.find(e => e.title === selectedValue).id;
    else
      return null;
 }

 ngOnDestroy(): void {
  this.RolesSubscription.unsubscribe();
  this.UsersSubscription.unsubscribe();
 }
}
