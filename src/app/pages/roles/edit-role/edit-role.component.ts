// import { Privilege } from './../../../models/privilege.model';
import { Privilege } from 'src/app/models/privilege.model'; 
// import { UserRole } from './../../../models/userRole.model';
import { UserRole } from 'src/app/models/userRole.model'; 
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MainService } from 'src/app/services/main.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.css'],
})
export class EditRoleComponent implements OnInit {

  userRoleId: number;
  userRole: UserRole;
  loading: boolean = false;
  roles : any[] = [];

  constructor(
              private mainService: MainService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private toastr: ToastrService,
              ) {
                  this.userRoleId = this.activatedRoute.snapshot.params['id'];                  
              }

  privileges: Privilege[] = [];

  role_privileges = [];

  form: FormGroup;
  selectedPrivilege = '';

  ngOnInit() {

    this.getPrivileges();
    
    this.form = new FormGroup({
      titleFormControl: new FormControl('', [Validators.required]),
      privilegesFormControl: new FormControl('', [Validators.required]),
      descriptionFormControl: new FormControl('', [Validators.required]),
    });
    
    this.getOneUserRole(this.userRoleId);  

  }

  submit() {
    let role_privileges_id = [];

    this.role_privileges.forEach((e) => {
      role_privileges_id.push({ privilege_id: e.id });
    });

    let userRole = {
      title: this.getFormControl('titleFormControl').value,
      privilege: role_privileges_id,
    };

    this.editUserRole(userRole, this.userRoleId);
  }

  addToRolePrivieges() {
    // @ts-ignore
    let privilegeId = this.getFormControl('privilegesFormControl').value;
    let privilegeItem = this.privileges.find((e) => e.id == +privilegeId);

    if(privilegeId != ""){
      if(!this.role_privileges.find(e => e.id == privilegeItem.id)){
        this.role_privileges.push(privilegeItem);
      }
    }
  }

  editUserRole(userRole: UserRole, id: number) {
    this.loading = true;
    this.mainService.editUserRole(userRole, id).subscribe(
      (res) => {
        this.loading = false;
        this.toastr.info("Successfully Edited a Role");
        this.router.navigate(['/roles']);
      },
      (err) => {
        alert(err);
      },
      () => {
        this.loading = false;
      }
    );
  }

  getPrivileges() {
    this.mainService.getAllPrivileges().subscribe(
      (res: any) => {
        this.privileges = res.data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getOneUserRole(id: number){
    this.mainService.getUserRoleById(id).subscribe(
      (res:any) => {
        this.userRole = res.data;
        this.form.get('titleFormControl').setValue(res.data.title);
        this.role_privileges = res.data.privileges;
      },
      err => {
        console.log(err);
      }
    );
  }

  getFormControl(name) {
    return this.form.get(name);
  }

  delete(item: string) {
    // @ts-ignore
    this.role_privileges.splice(this.role_privileges.indexOf(item, 1), 1);
  }
}
