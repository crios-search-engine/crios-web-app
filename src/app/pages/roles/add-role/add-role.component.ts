import { ToastrService } from 'ngx-toastr';
// import { UserRole } from './../../../models/userRole.model';
import { UserRole } from 'src/app/models/userRole.model';
// import { Privilege } from './../../../models/privilege.model';
import { Privilege } from 'src/app/models/privilege.model';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.css'],
})
export class AddRoleComponent implements OnInit {

  constructor(
              private mainService: MainService,
              private router: Router,
              private toastr: ToastrService
              ) {}

  privileges: Privilege[] = [];
  loading: boolean = false;
  role_privileges = [];


  form: FormGroup;
  selectedPrivilege = '';

  ngOnInit() {
    this.getPrivileges();
    this.form = new FormGroup({
      titleFormControl: new FormControl('', [Validators.required]),
      privilegesFormControl: new FormControl('', [Validators.required]),
    });
  }

  submit() {
    let role_privileges_id = [];

    this.role_privileges.forEach(e => {
      role_privileges_id.push({privilege_id: e.id});
    });

    let userRole = {
      title: this.getFormControl('titleFormControl').value,
      privilege: role_privileges_id,
    };

    this.addUserRole(userRole);

  }

  addToRolePrivieges() {
    // @ts-ignore
    let privilegeId = this.getFormControl('privilegesFormControl').value;
    let privilegeItem = this.privileges.find(e => e.id == +privilegeId);

    if (
      !this.role_privileges.includes(privilegeItem) &&
      privilegeItem != null
    ) {
      // @ts-ignore
      this.role_privileges.push(privilegeItem);
    }
  }

  addUserRole(userRole: UserRole){
    this.loading = true;
    this.mainService.addUserRole(userRole).subscribe(
      res => {
        this.toastr.success("Successfully Added a Role");
        this.loading = false;
        this.router.navigate(['/roles']);
      },
      err => {
        console.log(err);
      },
      () => {
        this.loading = false;
      }
    );
  }

  getPrivileges() {
    this.mainService.getAllPrivileges().subscribe(
      (res: any) => {
        this.privileges = res.data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getFormControl(name) {
    return this.form.get(name);
  }

  delete(item: string) {
    // @ts-ignore
    this.role_privileges.splice(this.role_privileges.indexOf(item, 1), 1);
  }
}
