// import { UserRole } from './../../../models/userRole.model';
import { UserRole } from 'src/app/models/userRole.model'; 
import { MainService } from 'src/app/services/main.service';
import {Component, OnInit} from '@angular/core';
import { ActiveToast, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-role',
  templateUrl: './view-role.component.html',
  styleUrls: ['./view-role.component.css']
})
export class ViewRoleComponent implements OnInit {

  loading: boolean = true;
  roles: UserRole[] = [];
  isEmptyRole: boolean = true;
  selectedRoleId: number;

  constructor(
              private mainService: MainService,
              private toastr: ToastrService,
            ) {
  }

  ngOnInit(): void {
    this.getRoles();
  }

  getRoles(){
    this.mainService.getAllRoles().subscribe(
      (res : any) =>{
        this.roles = res.data
        if(this.roles.length == 0)
          this.isEmptyRole = false;
      },
      err =>{
        console.log(err);
      },
      () => {
        this.loading = false
      }
    );
  }

  setRoleId(event){
    this.selectedRoleId = event;
  }

  deleteRole = (callback: Function): void|ActiveToast<ToastrService> =>{
    
    if(!this.selectedRoleId){
      return this.toastr.error("Something Went Wrong...");
    }
    this.mainService.deleteUserRole(this.selectedRoleId).subscribe(
      res => {
        this.getRoles();
        this.toastr.success("Successfully Deleted a Role");
        callback();
      },
      err => {
        console.log(err); 
      }
    );
  }
}
