import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsAllComponent } from './documents-all.component';

describe('DocumentsAllComponent', () => {
  let component: DocumentsAllComponent;
  let fixture: ComponentFixture<DocumentsAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentsAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
