import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-documents-all',
  templateUrl: './documents-all.component.html',
  styleUrls: ['./documents-all.component.css']
})
export class DocumentsAllComponent implements OnInit, OnDestroy {

  passDocumentUrl: any = '';
  passTopicTree: any;
  allDocuments = [];
  loading = false;
  DocumentsSubscription: Subscription = new Subscription();;

  constructor(
    private modalService: NgbModal,
    private sanitizer: DomSanitizer,
    private mainService: MainService
  ) { }

  ngOnInit(): void {
    this.getDocuments();
  }

  getDocuments(){
    this.loading = true;
    this.DocumentsSubscription = this.mainService.getAllDocuments().subscribe(
      (res:any) => {
        this.allDocuments = res;
        this.loading = false;
        console.log('all the document', res);
      },
      err => {
        console.log(err);
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openModalViewDocument(doc_url: any){
    this.passDocumentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(doc_url);
  }


  openModalViewTopicTree( tt: any){
    const modifiedTree = []
    tt.forEach(element => {
      console.log(element.topictree);
      modifiedTree.push(element.topictree[0])
    });

    this.passTopicTree = modifiedTree;
  }

  ngOnDestroy(): void {
    this.DocumentsSubscription.unsubscribe();
  }
}
