import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FlatTreeControl, NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener, MatTreeNestedDataSource} from '@angular/material/tree';
import { MainService } from 'src/app/services/main.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { ProfileTopicTreeFlatNode, ProfileTopicTreeWithCheck } from 'src/app/models/Profile.model';


@Component({
  selector: 'app-edit-document',
  templateUrl: './edit-document.component.html',
  styleUrls: ['./edit-document.component.css']
})
export class EditDocumentComponent implements OnInit, OnDestroy {

  docId: number;
  form: FormGroup;
  isLoading: boolean = false;
  user_id = "";
  DocumentByIdSubscription: Subscription = new Subscription();;
  document: any;
  modifiedTree = [];

  constructor(
              private mainService: MainService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private authService: AuthService,
              private toastr: ToastrService,
              ) {
    this.docId = this.activatedRoute.snapshot.params['id'];

  }

  saved_profile_topics = [];
  profile_topics = [];


  selectedPrivilege: string;



  ngOnInit(): void {
    this.user_id = this.authService.getUserInfo()?.id;

    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
    });


    this.getTopicTree();

    this.getDocumentByID(this.docId);
  }

  submit() {
    var urlencoded = new URLSearchParams();

    urlencoded.append("name", this.document?.name);
    urlencoded.append("title", this.title.value);
    urlencoded.append("path", this.document?.path);
    urlencoded.append("format", this.document?.format);
    urlencoded.append("description", this.description.value);
    urlencoded.append("status", this.document?.status);
    urlencoded.append("user_id", this.user_id);

    urlencoded.forEach(e => {
      console.log(e)

    })
    this.editDocument(urlencoded);
  }

  editDocument(documentToBeEdited){
    this.isLoading = true;
    this.mainService.editDocument(this.docId, documentToBeEdited).subscribe(
      res => {
        this.isLoading = false
        this.router.navigate(['/documents/mine']);
        this.toastr.info("Document Updated Successfully");
      },
      err => {
        this.isLoading = false
      },
      () => {
        this.isLoading = false
      }
    );
  }


  getDocumentByID(document_id: any){
    this.isLoading = true;
    this.DocumentByIdSubscription = this.mainService.getDocumentByID(document_id).subscribe(
      (res:any) => {
        this.document = res;

        console.log(this.document)

        this.document?.topics.forEach(element => {
          this.modifiedTree.push(element.topictree[0])
        });

        this.getExistedTopicTree(this.modifiedTree)

        this.saveExistedTopicTree()

        this.form.patchValue({
          title: res?.title,
          description: res?.description
        })

        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  addToRolePrivieges() {
    if (!this.profile_topics.includes(this.selectedPrivilege) && this.selectedPrivilege != null) {
      this.profile_topics.push(this.selectedPrivilege);
    }

  }

  delete(item: string) {
    this.profile_topics.splice(this.profile_topics.indexOf(item, 1), 1);
  }


  addToProfileTopics(event, id, name) {
    if(event.target.checked) {
      let checked = this.profile_topics.find(Item => Item.topictree_id === id);
      if(!checked){
        this.profile_topics.push({topictree_id: id, title: name});
        console.log(this.profile_topics)
      }else{
        console.log("already exists");
      }
    }else{
      console.log("unchecked")
      let checked = this.profile_topics.find(Item => Item.topictree_id === id);
      if(checked){
        this.profile_topics.forEach((element,index)=>{
          if(element.topictree_id == id) this.profile_topics.splice(index,1);
        });
        console.log(this.profile_topics)
      }
    }
  }


  saveChanges() {
    console.log(this.saved_profile_topics)
    this.saved_profile_topics = this.profile_topics;
  }

  getFormControl(name) {
    return this.form.get(name);
  }

  get title() {
    return this.form.get("title") as FormControl;
  }

  get description() {
    return this.form.get("description") as FormControl;
  }

  isChecked(id: any): boolean {
    return this.saved_profile_topics.some(e => e.topictree_id === id);
  }

  ngOnDestroy(): void {
    this.DocumentByIdSubscription.unsubscribe();
  }

  //for topic tree
  existedTopicTree: ProfileTopicTreeWithCheck[] = [];
  topicTree: ProfileTopicTreeWithCheck[] = [];
  initialTopicTree: ProfileTopicTreeWithCheck[] = [];
  savedTopicTree: ProfileTopicTreeWithCheck[] = [];
  savedExistedTopicTree: ProfileTopicTreeWithCheck[] = [];
  flattenedTopicTree: ProfileTopicTreeWithCheck[] = [];
  searchInProgress: boolean;
  hasMissingData: boolean = false;
  loading: boolean = true;
  finishedTopicTreeIDs: Array<{ topictree_id: string }> = []; // This one will be fulled on submit


  @ViewChild("closeModalBtn") closeModalBtn: ElementRef;

  treeControl = new FlatTreeControl<ProfileTopicTreeFlatNode>(({ level }) => level, ({ expandable }) => expandable);

    treeFlattener = new MatTreeFlattener(
    ({ id, topicparent_id, subtopics, checked, title }: ProfileTopicTreeWithCheck, level: number) => ({
      expandable: subtopics?.length > 0,
      id,
      topicparent_id,
      title,
      checked,
      level
    }),
    ({ level }) => level,
    ({ expandable }) => expandable,
    ({ subtopics }) => subtopics
  );

    dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  hasChild = (_: number, node: ProfileTopicTreeFlatNode) => node.expandable;
  isParent = (node: ProfileTopicTreeFlatNode) => node.level === 0;

  searchAndAddBranch = (leaf: ProfileTopicTreeWithCheck, branch: ProfileTopicTreeWithCheck): ProfileTopicTreeWithCheck => {
    if (this.searchInProgress) {
      if (Array.isArray(leaf.subtopics)) {
        if (branch.topicparent_id === leaf.id) {
          leaf.subtopics = [
            ...[...leaf.subtopics || []],
            branch
          ];
          this.searchInProgress = false;
        } else leaf.subtopics.forEach(leaf => this.searchAndAddBranch(leaf, branch));
      } else {
        if (branch.topicparent_id === leaf.id) {
          leaf.subtopics = [
            ...[...leaf.subtopics || []],
            branch
          ];
          this.searchInProgress = false;
        }
      }
    }
    return leaf;
  }

  addTopic = (branch: ProfileTopicTreeWithCheck): void => {
    if (!branch.topicparent_id) // if it's a parent branch
      this.topicTree.unshift(branch);
    else {
      this.searchInProgress = true;
      this.topicTree = this.topicTree.map(leaf => this.searchAndAddBranch(leaf, branch));
    }
  }

  addExistedTopic = (branch: ProfileTopicTreeWithCheck): void => {
    if (!branch.topicparent_id) // if it's a parent branch
      this.existedTopicTree.unshift(branch);
    else {
      this.searchInProgress = true;
      this.existedTopicTree = this.existedTopicTree.map(leaf => this.searchAndAddBranch(leaf, branch));
    }
  }

  addTopics = (topics: ProfileTopicTreeWithCheck[]): void => {
    if (topics.length > 0) {
      topics.forEach(topic => this.addTopic(topic));
      this.initialTopicTree = JSON.parse(JSON.stringify(this.topicTree));
    }
    this.dataSource.data = this.topicTree;
    this.searchInProgress = false;
  }

  addExistedTopics = (topics: ProfileTopicTreeWithCheck[]): void => {
    if (topics?.length > 0) {
      topics.forEach(topic => this.addExistedTopic(topic));
      // this.initialTopicTree = JSON.parse(JSON.stringify(this.existedTopicTree));
    }
    // this.dataSource.data = this.topicTree;
    // this.searchInProgress = false;
  }

  updateTopicTreeByCheckedValue = (branch: ProfileTopicTreeWithCheck, checked: boolean, id: number): ProfileTopicTreeWithCheck => {
    if (Array.isArray(branch.subtopics)) {
      if (id === branch.id) {
        branch.checked = checked;
        // Make beneath topic trees all true since it's a parent branch
        branch = this.drillDownTopicTreeAndCheckValue(branch, checked);
      } else branch.subtopics.forEach(leaf => this.updateTopicTreeByCheckedValue(leaf, checked, id));
    } else {
      if (id === branch.id)
        branch.checked = checked;
    }
    return branch;
  }

  drillDownTopicTreeAndCheckValue(branch: ProfileTopicTreeWithCheck, checked: boolean): ProfileTopicTreeWithCheck {
    if (Array.isArray(branch.subtopics)) {
      branch.checked = checked;
      branch.subtopics.forEach(leaf => this.drillDownTopicTreeAndCheckValue(leaf, checked));
    } else branch.checked = checked;
    return branch;
  }

  updateTopicTree({ target }: Event): void {
    const { value, checked } = <HTMLInputElement>target;
    console.log(this.topicTree)
    this.topicTree = this.topicTree.map(branch => this.updateTopicTreeByCheckedValue(branch, checked, parseInt(value)));
    this.updateDataSource();
  }

  updateExistedTopicTree(target: any): void {
    const { value, checked } = target;

    this.topicTree = this.topicTree.map(branch => this.updateTopicTreeByCheckedValue(branch, checked, parseInt(value)));
    this.updateDataSource();
  }

  updateDataSource(): void {
    this.dataSource.data = this.topicTree;
    this.expandTreeNodesByCheckedValue();
  }

  expandTreeNodesByCheckedValue(): void {
    this.treeControl.dataNodes.forEach((node, index) => {
      if (node.checked && node.expandable) this.treeControl.expand(node);
      if (node.checked) this.expandParentNodes(index, node.topicparent_id);
    });
  }

  expandParentNodes(index: number|null, parentID: number|null): void {
    if (parentID !== null) {
      for (let i = index; i > 0; i --) {
        if (this.treeControl.dataNodes[i].id === parentID) {
          this.treeControl.expand(this.treeControl.dataNodes[i]);
          if (this.treeControl.dataNodes[i].topicparent_id !== null)
            this.expandParentNodes(i, this.treeControl.dataNodes[i].topicparent_id);
        }
      }
    }
  }

  flatten = (branch: ProfileTopicTreeWithCheck): void => {
    if (Array.isArray(branch.subtopics)) {
      this.flattenedTopicTree.push(branch);
      branch.subtopics.forEach(branch => this.flatten(branch));
    } else
      this.flattenedTopicTree.push(branch);
  }

  // save only topics that were selected and return them as a topic tree (checked: true)
  // filterTopicTreeByCheckedValues(branch: ProfileTopicTreeWithCheck): boolean {
  // 	if (Array.isArray(branch.subtopics)) {
  // 		if (!branch.checked)
  // 			return false;
  // 		else branch.subtopics = branch.subtopics.filter(leaf => this.filterTopicTreeByCheckedValues(leaf));
  // 	} else {
  // 		if (!branch.checked)
  // 			return false;
  // 	}
  // 	return true;
  // }

  getIDsFromCheckedTopicTreeValues(branch: ProfileTopicTreeWithCheck): void {
    if (Array.isArray(branch.subtopics)) {
      if (branch.checked) { // If the parent is not checked, we don't have to look for children branched to add to the array
        this.finishedTopicTreeIDs.push({ topictree_id: String(branch.id) });
        branch.subtopics.forEach(branch => this.getIDsFromCheckedTopicTreeValues(branch));
      }
    } else {
      if (branch.checked){
        this.finishedTopicTreeIDs.push({ topictree_id: String(branch.id) });
      }
    }
  }

  saveTopicTree(): void {
    this.savedTopicTree = JSON.parse(JSON.stringify(this.topicTree));
    this.flattenSavedTopicTree();
    console.log(this.savedTopicTree)
  }

  saveExistedTopicTree(): void {
    this.checkExistedTopics();

    this.savedExistedTopicTree = JSON.parse(JSON.stringify(this.existedTopicTree));

    this.flattenExistedSavedTopicTree();
  }

  flattenSavedTopicTree(): void {
    this.flattenedTopicTree = [];
    this.savedTopicTree.forEach(branch => this.flatten(branch));
    if (this.flattenedTopicTree.length > 0 || this.flattenedTopicTree.some(({ checked }) => checked))
      this.hasMissingData = false;

    // edited
    this.flattenedTopicTree = this.flattenedTopicTree.filter((v,i,a)=>a.findIndex(v2=>(v2.id===v.id))===i)
  }

  flattenExistedSavedTopicTree(): void {
    this.flattenedTopicTree = [];
    this.savedExistedTopicTree.forEach(branch => this.flatten(branch));
    if (this.flattenedTopicTree.length > 0 || this.flattenedTopicTree.some(({ checked }) => checked))
      this.hasMissingData = false;

    this.flattenedTopicTree = this.flattenedTopicTree.filter((v,i,a)=>a.findIndex(v2=>(v2.id===v.id))===i)
  }

  cancelTopicTree(e?: Event|undefined): void {
    if (e) e.stopPropagation();
    if (!this.savedTopicTree.length)
      this.topicTree = JSON.parse(JSON.stringify(this.initialTopicTree));
    else this.topicTree = JSON.parse(JSON.stringify(this.savedTopicTree));
    this.checkExistedTopics();
    this.updateDataSource();
  }

  stopPropagation(e: Event): void {
    e.stopPropagation();
  }

  deleteTopic(id: number): void {
    this.topicTree = this.topicTree.map(branch => this.updateTopicTreeByCheckedValue(branch, false, id));
    this.saveTopicTree();
    this.updateDataSource();
  }

  // API Functions
  getTopicTree(): void {
    this.mainService.getTopicTree().subscribe(({ data = [] }) => {
      this.addTopics(
        (<ProfileTopicTreeWithCheck[]>data).map(obj => ({
          ...obj,
          checked: false
        }))
      );
    }, err => console.error(err), () => this.loading = false);
  }

  getExistedTopicTree(data): void {
    console.log(data);

    this.addExistedTopics(
      (<ProfileTopicTreeWithCheck[]> data)?.map(obj => ({
        ...obj,
        checked: true
      }))
    );
  }

  checkExistedTopics() {
    this.modifiedTree.forEach(e => {
      const target = {
        value: e.id,
        checked: true
      }

      this.updateExistedTopicTree(target)
    })
  }
}
