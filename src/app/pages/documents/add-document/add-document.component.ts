import { ToastrService, ActiveToast } from 'ngx-toastr';
import { ProfileTopicTreeWithCheck, ProfileTopicTreeFlatNode } from './../../../models/Profile.model';
import { AuthService } from './../../../services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FlatTreeControl, NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener, MatTreeNestedDataSource} from '@angular/material/tree';
import { MainService } from 'src/app/services/main.service';


@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.css']
})
export class AddDocumentComponent implements OnInit {

  file: any;
  formData = new FormData;
  selectedPrivilege: string;
  error: string;
  dragAreaClass: string;
  form: FormGroup;
  fName: any;
  isLoading: boolean = false;
  user_id = this.authService.getUserInfo()?.id;
  saved_profile_topics = [];
  profile_topics = [];

  constructor(
              private mainService: MainService,
              private router: Router,
              private http: HttpClient,
              private authService: AuthService,
              private toastr: ToastrService,
              ) {
  }


  ngOnInit() {
    this.dragAreaClass = 'dragarea';
    this.form = new FormGroup({
      titleFormControl: new FormControl('', [Validators.required]),
      descriptionFormControl: new FormControl('', [Validators.required]),
    });
    console.log(this.user_id)

    this.getTopicTree();
  }


  submit() {


    if (!this.flattenedTopicTree.length || !this.flattenedTopicTree.some(({ checked }) => checked))
			return this.hasMissingData = true;

		this.finishedTopicTreeIDs = [];
		this.savedTopicTree.forEach(branch => this.getIDsFromCheckedTopicTreeValues(branch));
		if (!this.finishedTopicTreeIDs.length) return this.toastr.error("Something Went Wrong...");

    this.finishedTopicTreeIDs = this.finishedTopicTreeIDs.filter((v,i,a)=>a.findIndex(v2=>(v2.topictree_id===v.topictree_id))===i)

		console.log("tree", this.finishedTopicTreeIDs);

    const { titleFormControl, descriptionFormControl } = this.form.value;

    //prepare document
    let postDocument = {
      title: titleFormControl,
      description: descriptionFormControl,
      file: this.formData,
      status: 1,
      name: this.fName,
      // topictree: this.profile_topics,
      topictree: [2],
      user_id: this.user_id
      // user_id: '56f7ed4a-dbb4-4e7d-bb63-7f272e697d65'
    }
    this.formData.append("title", titleFormControl);
    this.formData.append("description", descriptionFormControl);
    this.formData.append("status", "1");
    this.formData.append("name", this.fName);
    // this.formData.append("topictree[0]", "2");
    this.formData.append("user_id", this.user_id);

    this.finishedTopicTreeIDs.forEach((e, i) => {
      this.formData.append(`topictree[${i}]`, e.topictree_id);
    });

    console.log(postDocument)
    this.addDocument(this.formData);
  }

  addDocument(documentToBeAdded){
    this.isLoading = true;
    this.mainService.postDocument(documentToBeAdded).subscribe(
      res => {
        this.isLoading = false;
        this.toastr.success("Document Added Successfully");
        console.log(res);
        this.clearData();
      },
      err => {
        console.log(err)
        this.isLoading = false
      },
      () => {
        this.isLoading = false
      }
    );
  }

  addToRolePrivieges() {
    if (!this.profile_topics.includes(this.selectedPrivilege) && this.selectedPrivilege != null) {
      this.profile_topics.push(this.selectedPrivilege);
    }

  }

  delete(item: string) {
    this.profile_topics.splice(this.profile_topics.indexOf(item, 1), 1);
    console.log(this.profile_topics)
  }

  clearData() {
    this.form.reset();
    this.formData.delete;
    this.fName = null;
    this.finishedTopicTreeIDs = [];
    this.flattenedTopicTree = [];
  }

  //file_input: any;



  // addToProfileTopics(checked: Event, $any: any) {
  //   if (checked) {
  //     this.profile_topics.push(checked.target['value']);
  //     console.log(checked.target['value'])
  //   } else {
  //     this.profile_topics.splice(this.profile_topics.indexOf(checked.target['value'], 1), 1);

  //   }

  // }
  addToProfileTopics(event, id, name) {
    if(event.target.checked) {
      let checked = this.profile_topics.find(Item => Item.topictree_id === id);
      if(!checked){
        this.profile_topics.push({topictree_id: id, title: name});
        console.log(this.profile_topics)
      }else{
        console.log("already exists");
      }
    }else{
      console.log("unchecked")
      let checked = this.profile_topics.find(Item => Item.topictree_id === id);
      if(checked){
        this.profile_topics.forEach((element,index)=>{
          if(element.topictree_id == id) this.profile_topics.splice(index,1);
        });
        console.log(this.profile_topics)
      }
    }
  }

  saveChanges() {
    this.saved_profile_topics = this.profile_topics;
  }

  onFileChange(event: any) {
    const files: FileList = event.target.files;
    this.saveFiles(files);
  }


  @HostListener('dragover', ['$event']) onDragOver(event: any) {
    event.preventDefault();
  }

  @HostListener('dragenter', ['$event']) onDragEnter(event: any) {
    event.preventDefault();
  }

  @HostListener('dragend', ['$event']) onDragEnd(event: any) {
    event.preventDefault();
  }

  @HostListener('dragleave', ['$event']) onDragLeave(event: any) {
    event.preventDefault();
  }

  @HostListener('drop', ['$event']) onDrop(event: any) {
    event.preventDefault();
    event.stopPropagation();
    if (event.dataTransfer.files) {
      const files: FileList = event.dataTransfer.files;
      this.saveFiles(files);
    }
  }

  saveFiles(files: FileList) {

    if (files.length > 1) {
      this.error = 'Only one file at time allow';
    } else {
      this.error = '';
      if(files[0] != undefined){
        this.fName = files[0].name;
        this.file = files[0];

        this.formData = new FormData()
        this.formData.append('file', this.file, this.fName);
      }
    }
  }

  getFormControl(name) {
    return this.form.get(name);
  }


  //for topic tree
  topicTree: ProfileTopicTreeWithCheck[] = [];
	initialTopicTree: ProfileTopicTreeWithCheck[] = [];
	savedTopicTree: ProfileTopicTreeWithCheck[] = [];
	flattenedTopicTree: ProfileTopicTreeWithCheck[] = [];
  searchInProgress: boolean;
  hasMissingData: boolean = false;
  loading: boolean = true;
  finishedTopicTreeIDs: Array<{ topictree_id: string }> = []; // This one will be fulled on submit

  @ViewChild("closeModalBtn") closeModalBtn: ElementRef;

  treeControl = new FlatTreeControl<ProfileTopicTreeFlatNode>(({ level }) => level, ({ expandable }) => expandable);

  	treeFlattener = new MatTreeFlattener(
		({ id, topicparent_id, subtopics, checked, title }: ProfileTopicTreeWithCheck, level: number) => ({
			expandable: subtopics?.length > 0,
			id,
			topicparent_id,
			title,
			checked,
			level
		}),
		({ level }) => level,
		({ expandable }) => expandable,
		({ subtopics }) => subtopics
	);

  	dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

	hasChild = (_: number, node: ProfileTopicTreeFlatNode) => node.expandable;
	isParent = (node: ProfileTopicTreeFlatNode) => node.level === 0;

	searchAndAddBranch = (leaf: ProfileTopicTreeWithCheck, branch: ProfileTopicTreeWithCheck): ProfileTopicTreeWithCheck => {
		if (this.searchInProgress) {
			if (Array.isArray(leaf.subtopics)) {
				if (branch.topicparent_id === leaf.id) {
					leaf.subtopics = [
						...[...leaf.subtopics || []],
						branch
					];
					this.searchInProgress = false;
				} else leaf.subtopics.forEach(leaf => this.searchAndAddBranch(leaf, branch));
			} else {
				if (branch.topicparent_id === leaf.id) {
					leaf.subtopics = [
						...[...leaf.subtopics || []],
						branch
					];
					this.searchInProgress = false;
				}
			}
		}
		return leaf;
	}
	addTopic = (branch: ProfileTopicTreeWithCheck): void => {
		if (!branch.topicparent_id) // if it's a parent branch
			this.topicTree.unshift(branch);
		else {
			this.searchInProgress = true;
			this.topicTree = this.topicTree.map(leaf => this.searchAndAddBranch(leaf, branch));
		}
	}
	addTopics = (topics: ProfileTopicTreeWithCheck[]): void => {
		if (topics.length > 0) {
			topics.forEach(topic => this.addTopic(topic));
			this.initialTopicTree = JSON.parse(JSON.stringify(this.topicTree));
		}
		this.dataSource.data = this.topicTree;
		this.searchInProgress = false;
	}

	updateTopicTreeByCheckedValue = (branch: ProfileTopicTreeWithCheck, checked: boolean, id: number): ProfileTopicTreeWithCheck => {
		if (Array.isArray(branch.subtopics)) {
			if (id === branch.id) {
				branch.checked = checked;
				// Make beneath topic trees all true since it's a parent branch
				branch = this.drillDownTopicTreeAndCheckValue(branch, checked);
			} else branch.subtopics.forEach(leaf => this.updateTopicTreeByCheckedValue(leaf, checked, id));
		} else {
			if (id === branch.id)
				branch.checked = checked;
		}
		return branch;
	}

	drillDownTopicTreeAndCheckValue(branch: ProfileTopicTreeWithCheck, checked: boolean): ProfileTopicTreeWithCheck {
		if (Array.isArray(branch.subtopics)) {
			branch.checked = checked;
			branch.subtopics.forEach(leaf => this.drillDownTopicTreeAndCheckValue(leaf, checked));
		} else branch.checked = checked;
		return branch;
	}

	updateTopicTree({ target }: Event): void {
		const { value, checked } = <HTMLInputElement>target;
		this.topicTree = this.topicTree.map(branch => this.updateTopicTreeByCheckedValue(branch, checked, parseInt(value)));
		this.updateDataSource();
	}

	updateDataSource(): void {
		this.dataSource.data = this.topicTree;
		this.expandTreeNodesByCheckedValue();
	}

	expandTreeNodesByCheckedValue(): void {
		this.treeControl.dataNodes.forEach((node, index) => {
			if (node.checked && node.expandable) this.treeControl.expand(node);
			if (node.checked) this.expandParentNodes(index, node.topicparent_id);
		});
	}

	expandParentNodes(index: number|null, parentID: number|null): void {
		if (parentID !== null) {
			for (let i = index; i > 0; i --) {
				if (this.treeControl.dataNodes[i].id === parentID) {
					this.treeControl.expand(this.treeControl.dataNodes[i]);
					if (this.treeControl.dataNodes[i].topicparent_id !== null)
						this.expandParentNodes(i, this.treeControl.dataNodes[i].topicparent_id);
				}
			}
		}
	}

	flatten = (branch: ProfileTopicTreeWithCheck): void => {
		if (Array.isArray(branch.subtopics)) {
			this.flattenedTopicTree.push(branch);
			branch.subtopics.forEach(branch => this.flatten(branch));
		} else
			this.flattenedTopicTree.push(branch);
	}

	// save only topics that were selected and return them as a topic tree (checked: true)
	// filterTopicTreeByCheckedValues(branch: ProfileTopicTreeWithCheck): boolean {
	// 	if (Array.isArray(branch.subtopics)) {
	// 		if (!branch.checked)
	// 			return false;
	// 		else branch.subtopics = branch.subtopics.filter(leaf => this.filterTopicTreeByCheckedValues(leaf));
	// 	} else {
	// 		if (!branch.checked)
	// 			return false;
	// 	}
	// 	return true;
	// }

	getIDsFromCheckedTopicTreeValues(branch: ProfileTopicTreeWithCheck): void {
		if (Array.isArray(branch.subtopics)) {
			if (branch.checked) { // If the parent is not checked, we don't have to look for children branched to add to the array
				this.finishedTopicTreeIDs.push({ topictree_id: String(branch.id) });
				branch.subtopics.forEach(branch => this.getIDsFromCheckedTopicTreeValues(branch));
			}
		} else {
			if (branch.checked){
				this.finishedTopicTreeIDs.push({ topictree_id: String(branch.id) });
      }
		}
	}

	saveTopicTree(): void {
		this.savedTopicTree = JSON.parse(JSON.stringify(this.topicTree));
		this.flattenSavedTopicTree();
	}

	flattenSavedTopicTree(): void {
		this.flattenedTopicTree = [];
		this.savedTopicTree.forEach(branch => this.flatten(branch));
		if (this.flattenedTopicTree.length > 0 || this.flattenedTopicTree.some(({ checked }) => checked))
			this.hasMissingData = false;

    this.flattenedTopicTree = this.flattenedTopicTree.filter((v,i,a)=>a.findIndex(v2=>(v2.id===v.id))===i)
	}

	cancelTopicTree(e?: Event|undefined): void {
		if (e) e.stopPropagation();
		if (!this.savedTopicTree.length)
			this.topicTree = JSON.parse(JSON.stringify(this.initialTopicTree));
		else this.topicTree = JSON.parse(JSON.stringify(this.savedTopicTree));
		this.updateDataSource();
	}

	stopPropagation(e: Event): void {
		e.stopPropagation();
	}

	deleteTopic(id: number): void {
		this.topicTree = this.topicTree.map(branch => this.updateTopicTreeByCheckedValue(branch, false, id));
		this.saveTopicTree();
		this.updateDataSource();
	}

	// API Functions
	getTopicTree(): void {
		this.mainService.getTopicTree().subscribe(({ data = [] }) => {
			this.addTopics(
				(<ProfileTopicTreeWithCheck[]>data).map(obj => ({
					...obj,
					checked: false
				}))
			);
		}, err => console.error(err), () => this.loading = false);
	}

	// submitt(): void|boolean|ActiveToast<ToastrService> {
	// 	if (!this.flattenedTopicTree.length || !this.flattenedTopicTree.some(({ checked }) => checked))
	// 		return this.hasMissingData = true;

	// 	const { titleFormControl, descriptionFormControl } = this.form.value;

	// 	this.finishedTopicTreeIDs = [];
	// 	this.savedTopicTree.forEach(branch => this.getIDsFromCheckedTopicTreeValues(branch));
	// 	if (!this.finishedTopicTreeIDs.length) return this.toastr.error("Something Went Wrong...");
	// 	console.log(this.finishedTopicTreeIDs);

	// }
}
