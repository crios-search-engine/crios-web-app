import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsMineComponent } from './documents-mine.component';

describe('DocumentsMineComponent', () => {
  let component: DocumentsMineComponent;
  let fixture: ComponentFixture<DocumentsMineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentsMineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsMineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
