import { ToastrService } from 'ngx-toastr';
import { AuthService } from './../../../services/auth.service';
import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MainService } from 'src/app/services/main.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-documents-mine',
  templateUrl: './documents-mine.component.html',
  styleUrls: ['./documents-mine.component.css']
})
export class DocumentsMineComponent implements OnInit, OnDestroy {

  passTopicTree: any;
  doc_id: number;
  status = true;
  disableHide = false;
  passDocumentUrl: any = '';
  loading = false;
  user_id = '';
  DocumentsSubscription: Subscription = new Subscription();;
  // files: any[] = [
  //   {
  //     document_id: 1,
  //     document: "sample01.pdf",
  //     title: "title01",
  //     name: "khaled",
  //     document_url: "https://myhostingprojects.000webhostapp.com/images/AI%20Excercises%20Sheet%201.pdf",
  //     status: 0,
  //     topictree: [
  //       {
  //         name: 'topic1',
  //         children: [
  //           {name: 'Apple'},
  //           {name: 'Banana'},
  //           {name: 'Fruit loops'},
  //         ]
  //       }, {
  //         name: 'topic2',
  //         children: [
  //           {
  //             name: 'Green',
  //             children: [
  //               {name: 'Broccoli'},
  //               {name: 'Brussels sprouts'},
  //             ]
  //           }, {
  //             name: 'Orange',
  //             children: [
  //               {name: 'Pumpkins'},
  //               {name: 'Carrots'},
  //             ]
  //           },
  //         ]
  //       },
  //     ]
  //   },
  //   {
  //     document_id: 2,
  //     document: "sample02.pdf",
  //     title: "title02",
  //     name: "khaled",
  //     document_url: "https://ia601305.us.archive.org/26/items/IntermediatePython/IntermediatePython.pdf",
  //     status: 2,
  //     topictree: [
  //       {
  //         name: 'laptop',
  //         children: [
  //           {name: 'lenovo'},
  //           {name: 'hp'},
  //           {name: 'dell'},
  //         ]
  //       }, {
  //         name: 'phone',
  //         children: [
  //           {
  //             name: 'iphone12',
  //             children: [
  //               {name: 'red'},
  //               {name: 'gold'},
  //             ]
  //           }
  //         ]
  //       },
  //     ]
  //   },
  //   {
  //     document_id: 3,
  //     document: "sample03.pdf",
  //     title: "title03",
  //     name: "khaled",
  //     document_url: "https://myhostingprojects.000webhostapp.com/images/AI%20Exercises%20Sheet%205%20-%20minimax.pdf",
  //     status: 2,
  //     topictree: [
  //       {
  //         name: 'front-end',
  //         children: [
  //           {name: 'angular'},
  //           {name: 'vue'},
  //           {name: 'reactjs'},
  //         ]
  //       }, {
  //         name: 'backend',
  //         children: [
  //           {
  //             name: 'frameworks',
  //             children: [
  //               {name: 'express'},
  //               {name: 'laravel'},
  //             ]
  //           }, {
  //             name: 'pure',
  //             children: [
  //               {name: 'nodejs'},
  //               {name: 'php'},
  //             ]
  //           },
  //         ]
  //       },
  //       {
  //         name: 'full-stack',
  //         children: [
  //           {name: 'angular'},
  //           {name: 'express'},
  //           {name: 'mongodb'},
  //           {name: 'nodejs'},
  //         ]
  //       }
  //     ]
  //   },
  //   {
  //     document_id: 4,
  //     document: "sample04.pdf",
  //     title: "title04",
  //     name: "khaled",
  //     document_url: "https://myhostingprojects.000webhostapp.com/images/AI%20Exercises%20Sheet%203.pdf",
  //     status: 1,
  //     topictree: [
  //       {
  //         name: 'AI',
  //         children: [
  //           {name: 'machine'},
  //           {name: 'deep learning'},
  //         ]
  //       }
  //     ]
  //   },
  // ];

  src = 'assets/My Documents Page/image2vector.svg';
  documents = [];

  constructor(
              private modalService: NgbModal,
              private mainService: MainService,
              private authService: AuthService,
              private sanitizer: DomSanitizer,
              private toastr: ToastrService
             ) { }

  ngOnInit(): void {
    this.user_id = this.authService.getUserInfo().id;
    this.getDocumentsMine(this.user_id);
  }

  getDocumentsMine(userId){
    this.loading = true;
    this.DocumentsSubscription = this.mainService.getDocumentByUser(userId).subscribe(
      (res : any) => {
        console.log("My documents", res);
        this.documents = res;
        this.loading = false;
      },
      err =>  {
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  delete(callBack: Function){
    console.log("main delete function",this.doc_id);
    callBack();
    this.mainService.deleteDocument(this.doc_id).subscribe(
      res => {
        console.log(res);
        this.getDocumentsMine(this.user_id);
        this.toastr.success(res);
        callBack();
      },
      err => {

      }
    );
  }

  clickEvent(document_id: number) {
    console.log(document_id)
    this.disableHide = true;
    this.mainService.hideDocument(document_id).subscribe(
      res =>{
        this.disableHide = false;
        console.log(res);
        this.getDocumentsMine(this.user_id);
        this.toastr.info(res);
        },
        err => {
        this.disableHide = false;
        },
        () => {
          this.disableHide = false;
      }
    );
  }


  openModalDeleteDocument(id: number){
    this.doc_id = id;
    console.log(id);
  }

  openModalViewDocument(doc_url: any){
    this.passDocumentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(doc_url);
    console.log(this.passDocumentUrl)
  }

  openModalViewTopicTree( tt: any){
    const modifiedTree = []
    tt.forEach(element => {
      console.log(element.topictree);
      modifiedTree.push(element.topictree[0])
    });

    this.passTopicTree = modifiedTree;
  }

  ngOnDestroy(): void {
    this.DocumentsSubscription.unsubscribe();
  }
}
