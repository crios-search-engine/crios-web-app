import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  constructor(
    private service: AuthService,
    private router: Router,
    private socialAuthService: SocialAuthService
  ) {}

  checkMatchValidator(field1: string, field2: string) {
    this.equal = field1 == field2;
  }

  equal: boolean = false;
  countries = [];
  signUpSuccess: boolean = false;
  option = true;
  fromGoogle: boolean = false;
  pass = '';
  googleMail = '';
  password = '';
  repassword = '';
  iemail: any;
  bio: string = '';
  existingEmail: boolean = false;
  nonEligibleEmail: boolean = false;

  signUpForm = new FormGroup({
    passwordFormControl: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    repasswordFormControl: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
  });

  form = new FormGroup({
    fNameFormControl: new FormControl('', [
      Validators.required,
      Validators.maxLength(32),
    ]),
    lNameFormControl: new FormControl('', [
      Validators.required,
      Validators.maxLength(32),
    ]),
    gender: new FormControl('', [Validators.required]),
    phoneFormControl: new FormControl('', [Validators.maxLength(16)]),
    mobileFormControl: new FormControl('', [
      Validators.required,
      Validators.maxLength(16),
    ]),
    countryFormControl: new FormControl('', [
      Validators.required,
      Validators.maxLength(20),
    ]),
    addressFormControl: new FormControl('', [
      Validators.required,
      Validators.maxLength(255),
    ]),
    birthdayFormControl: new FormControl('', [Validators.required]),
    biographyFormControl: new FormControl('', [Validators.maxLength(255)]),
    backupEmailFormControl: new FormControl('', [
      Validators.maxLength(64),
      Validators.email,
    ]),
  });

  ngOnInit() {
    this.service.getCountry().then((data: any) => {
      data.data.forEach((element) => {
        this.countries.push({ id: element.id, name: element.name });
      });
    });
  }

  email() {
    this.option = true;
    this.existingEmail = false;
    this.nonEligibleEmail = false;
  }

  gmail() {
    this.option = false;
    this.existingEmail = false;
    this.nonEligibleEmail = false;
  }

  changeEmail() {
    this.existingEmail = false;
    this.nonEligibleEmail = false;
  }

  signUpRequest() {
    if (this.signUpForm.valid) {
      this.fromGoogle = false;
      this.service.checkEligibility(this.iemail).then((res: any) => {
        if (res.eligible) this.signUpSuccess = !this.signUpSuccess;
        else if (res.registered) {
          this.existingEmail = true;
        } else this.nonEligibleEmail = true;
      });
    }
  }

  signUpWithGoogle() {
    this.socialAuthService
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((data: any) => {
        this.pass = data.id;
        this.googleMail = data.email;
        this.fromGoogle = true;
        this.service.checkEligibility(data.email).then((res: any) => {
          if (res.eligible) this.signUpSuccess = !this.signUpSuccess;
          else if (res.registered) this.existingEmail = true;
          else this.nonEligibleEmail = true;
        });
      });
  }

  saveAccount() {
    let json = {
      email: this.fromGoogle
        ? this.googleMail
        : this.signUpForm.controls.emailFormControl.value,
      password: this.fromGoogle
        ? this.pass
        : this.signUpForm.controls.passwordFormControl.value,
      firstname: this.form.controls.fNameFormControl.value,
      lastname: this.form.controls.lNameFormControl.value,
      mobile: this.form.controls.mobileFormControl.value,
      landline: this.form.controls.phoneFormControl.value,
      country_id: this.form.controls.countryFormControl.value,
      address: this.form.controls.addressFormControl.value,
      birthdate: this.form.controls.birthdayFormControl.value,
      status: 1,
      biography: this.form.controls.biographyFormControl.value,
      secondemail: this.form.controls.backupEmailFormControl.value,
      gender: this.form.controls.gender.value == 'Male' ? 1 : 2,
    };
    if (this.form.valid)
      this.service.signUp(json).then((res: any) => {
        localStorage.setItem(
          'user_data',
          JSON.stringify(res)
        );
        this.router.navigate(['/home']);
      });
  }
}
