import { User } from 'src/app/models/user.model';  
import { Country } from 'src/app/models/country.model'; 
import { MainService } from 'src/app/services/main.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css']
})
export class EditAccountComponent implements OnInit {

  loading: boolean = false;
  user_data = this.authService.getUserInfo();
  countries: Country[] = [];

  form = new FormGroup({
    fNameFormControl: new FormControl('', [Validators.required]),
    lNameFormControl: new FormControl('', [Validators.required]),
    phoneFormControl: new FormControl(''),
    mobileFormControl: new FormControl('', [Validators.required]),
    countryFormControl: new FormControl('', [Validators.required]),
    addressFormControl: new FormControl('', [Validators.required]),
    birthdayFormControl: new FormControl('', [Validators.required]),
    biographyFormControl: new FormControl(''),
    genderFormControl: new FormControl('', [Validators.required]),
    backupEmailFormControl: new FormControl('', [Validators.email]),

  });

  constructor(
              private activatedRoute: ActivatedRoute,
              private mainService: MainService,
              private authService: AuthService,
              private router: Router,
              private toastr: ToastrService,
              ) { 

                }

  ngOnInit(): void {
    this.form.get("fNameFormControl").setValue(this.user_data.firstname);
    this.form.get("lNameFormControl").setValue(this.user_data.lastname);
    this.form.get("phoneFormControl").setValue(this.user_data.landline);
    this.form.get("mobileFormControl").setValue(this.user_data.mobile);
    this.form.get("addressFormControl").setValue(this.user_data.address);
    this.form.get("birthdayFormControl").setValue(this.user_data.birthdate);
    this.form.get("biographyFormControl").setValue(this.user_data.biography);
    this.form.get("backupEmailFormControl").setValue(this.user_data.secondemail);
    this.form.get("countryFormControl").setValue(this.user_data.country.id);
    this.form.get("genderFormControl").setValue(this.user_data.gender == 1 ? "Male": "Female");
    this.getCountries();
  }

  getCountries(){
    this.authService.getCountry().then((res: any) => {
      this.countries = res.data;
    });
  }

  submit() {
    let user_id = this.user_data.id;
    let editedAcount: User = {
      firstname: this.form.get("fNameFormControl").value,
      lastname: this.form.get("lNameFormControl").value,
      mobile: this.form.get("mobileFormControl").value,
      landline: this.form.get("phoneFormControl").value,
      address: this.form.get("addressFormControl").value,
      birthdate: this.form.get("birthdayFormControl").value,
      biography: this.form.get("biographyFormControl").value,
      secondemail: this.form.get("backupEmailFormControl").value,
      country: this.form.get("countryFormControl").value,
      gender: this.form.get("genderFormControl").value == 'Male'? 1 : 2,
    }
    if(this.form.valid){
      this.loading = true;
      this.mainService.editAcount(editedAcount, user_id).subscribe(
        (res: any) => {
          if(res)
            {
              var newUserData = JSON.parse(this.authService.getLocalStorage());
              newUserData.data = res.data;
              //set new user_data in localstorage
              localStorage.setItem("user_data", JSON.stringify(newUserData));
              this.loading = false;
              this.toastr.info("Successfully Edited Account");
              this.router.navigate(['/accounts/' + user_id])
            }
        },
        () => {
          this.loading = false;
        }
      );
    }
  }

  isSelected(countryId): boolean { 
    return this.user_data.country.id === countryId;
  }
}
