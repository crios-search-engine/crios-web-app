import { MainService } from 'src/app/services/main.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Eligible } from 'src/app/models/eligible.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-eligible',
  templateUrl: './eligible.component.html',
  styleUrls: ['./eligible.component.css']
})
export class EligibleComponent implements OnInit, OnDestroy {

  loading: boolean = true;
  eligibleUsers: Eligible[] = [];
  EligbleSubscription: Subscription = new Subscription();;

  constructor(private mainService: MainService) { }

  ngOnInit(): void {
    this.getEligibleUsers();
  }

  getEligibleUsers(){
    this.EligbleSubscription = this.mainService.getEligibleUsers().subscribe(
      (res: any) => {
        this.eligibleUsers = res.data;
        this.loading = false;
      },
      err => {
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  ngOnDestroy(): void {
    this.EligbleSubscription.unsubscribe();
  }
}
