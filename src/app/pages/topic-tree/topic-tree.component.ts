import { Component, OnInit } from "@angular/core";
import { MatTreeFlatDataSource, MatTreeFlattener } from "@angular/material/tree";
import { FlatTreeControl } from "@angular/cdk/tree";
import TopicTree, { TopicTreeFlatNode } from "../../models/TopicTree.model";
import { MainService } from "src/app/services/main.service";
import { ActiveToast, ToastrService } from "ngx-toastr";
import TopicRequest from "src/app/models/TopicRequest.model";
import { forkJoin } from "rxjs";

@Component({
	selector: "app-topic-tree",
	templateUrl: "./topic-tree.component.html",
	styleUrls: ["./topic-tree.component.css"]
})

export class TopicTreeComponent implements OnInit {

	isAnEditTopicRequest: boolean;

	loading: boolean = true;
	topicRequestLoading: boolean;
	searchInProgress: boolean;

	search: string;
	selectedNodeID: number;
	selectedTitle: string;
	selectedParentID: number|null;

	topicTree: TopicTree[] = [];
	filteredTree: TopicTree[] = [];
	topicRequests: TopicRequest[] = [];
	removedBranch: TopicTree;

  	treeControl = new FlatTreeControl<TopicTreeFlatNode>(({ level }) => level, ({ expandable }) => expandable);

  	treeFlattener = new MatTreeFlattener(
		({ id, topicparent_id, subtopics, title }: TopicTree, level: number) => ({
			expandable: subtopics?.length > 0,
			id,
			topicparent_id,
			title,
			level
		}), 
		({ level }) => level, 
		({ expandable }) => expandable, 
		({ subtopics }) => subtopics
	);

  	dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

	constructor(private API: MainService, private toastr: ToastrService) { }

	ngOnInit(): void { 
		forkJoin([
			this.API.getTopicTree(),
			this.API.getTopicRequests()
		]).subscribe(([topics, topicRequests]) => {
			console.log(topics) 
			this.addTopics(<TopicTree[]>topics.data);
			this.topicRequests = topicRequests.data as TopicRequest[];
		}, err => console.error(err), () => this.loading = false);
	}

	filterBranch = (branch: TopicTree, search: string): void => {
		if (Array.isArray(branch.subtopics)) { 
			if (branch.title.toLowerCase().includes(search)) 
				this.filteredTree.push(branch);
			else branch.subtopics.forEach(branch => this.filterBranch(branch, search));
		} else {
			if (branch.title.toLowerCase().includes(search)) 
				this.filteredTree.push(branch);
		}
	}
	searchAndAddBranch = (leaf: TopicTree, branch: TopicTree): TopicTree => { 
		if (this.searchInProgress) { 
			if (Array.isArray(leaf.subtopics)) { 
				if (branch.topicparent_id === leaf.id) {
					leaf.subtopics = [
						...[...leaf.subtopics || []], 
						branch
					];
					this.searchInProgress = false;
				} else leaf.subtopics.forEach(leaf => this.searchAndAddBranch(leaf, branch));
			} else {
				if (branch.topicparent_id === leaf.id) { 
					leaf.subtopics = [
						...[...leaf.subtopics || []], 
						branch
					];
					this.searchInProgress = false;
				}
			}
		}
		return leaf;
	}
	// addBranchByID = (branch: TopicTree, title: string): TopicTree => {
	// 	if (Array.isArray(branch.subtopics)) { 
	// 		if (branch.id === this.selectedNodeID) 
	// 			branch.subtopics = [
	// 				...[...branch.subtopics || []], 
	// 				{
	// 					id: Date.now(),
	// 					title,
	// 					status: 2,
	// 					topicparent_id: this.selectedNodeID,
	// 				}
	// 			];
	// 		else branch.subtopics.forEach(leaf => this.addBranchByID(leaf, title));
	// 	} else {
	// 		if (branch.id === this.selectedNodeID) branch.subtopics = [
	// 			...[...branch.subtopics || []], 
	// 			{
	// 				id: Date.now(),
	// 				title,
	// 				status: 2,
	// 				topicparent_id: this.selectedNodeID,
	// 			}
	// 		];
	// 	}
	// 	return branch;
	// }
	// searchAndEditBranch = (leaf: TopicTree, branch: TopicTree): TopicTree => {
	// 	if (Array.isArray(leaf.subtopics)) { 
	// 		if (this.selectedNodeID === leaf.id) 
	// 			leaf.title = branch.title;
	// 		else leaf.subtopics.forEach(leaf => this.searchAndEditBranch(leaf, branch));
	// 	} else {
	// 		if (this.selectedNodeID === leaf.id) 
	// 			leaf.title = branch.title;
	// 	}
	// 	return leaf;
	// }
	// removeBranchByID = (branch: TopicTree): boolean => {
	// 	if (Array.isArray(branch.subtopics)) { 
	// 		if (branch.id === this.selectedNodeID) {
	// 			this.removedBranch = branch;
	// 			return false;
	// 		} else { 
	// 			const filteredChildren = branch.subtopics.filter(leaf => this.removeBranchByID(leaf));
	// 			if (!filteredChildren.length) delete branch.subtopics; // if it does not have children, remove children attribute
	// 			else branch.subtopics = filteredChildren; // update branch children
	// 		}
	// 	} else {
	// 		if (branch.id === this.selectedNodeID) {
	// 			this.removedBranch = branch;
	// 			return false;
	// 		}
	// 	}
	// 	return true;
	// }

	hasChild = (_: number, node: TopicTreeFlatNode) => node.expandable;
	isParent = (node: TopicTreeFlatNode) => node.level === 0;

	searchTree = (): void => {
		this.filteredTree = [];
		this.topicTree.forEach(branch => this.filterBranch(branch, this.search.toLowerCase()));
		this.dataSource.data = this.filteredTree;
	}

	onSearchChange = (): void => {
		if (!this.search)
			this.dataSource.data = this.topicTree;
	}

	addTopic = (branch: TopicTree): void => {
		if (!branch.topicparent_id) // if it's a parent branch
			this.topicTree.unshift(branch);
		else { 
			this.searchInProgress = true;
			this.topicTree = this.topicTree.map(leaf => this.searchAndAddBranch(leaf, branch));
		}
	}
	addTopics = (topics: TopicTree[]): void => { 
		this.topicTree = [];
		if (topics.length > 0) topics.forEach(topic => this.addTopic(topic));
		this.dataSource.data = this.topicTree;
		this.searchInProgress = false;
	}
	// editTopic = (branch: TopicTree): void => {
	// 	if (branch.topicparent_id === this.selectedParentID) { // Only title will be edited since its parent ID hasn't changed
	// 		this.topicTree = this.topicTree.map(leaf => this.searchAndEditBranch(leaf, branch));
	// 		this.dataSource.data = this.topicTree;
	// 	} else { // current branch will be removed and added again
	// 		this.topicTree = this.topicTree.filter(branch => this.removeBranchByID(branch));
	// 		if (this.removedBranch?.subtopics)
	// 			this.removedBranch = { 
	// 				...branch,
	// 				subtopics: this.removedBranch.subtopics
	// 			}; 
	// 		else this.removedBranch = branch;
	// 		this.addTopic(this.removedBranch);
	// 		this.dataSource.data = this.topicTree;
	// 	}
	// }
	// addSubTopic = (title: string): void => {
	// 	this.topicTree = this.topicTree.map(branch => this.addBranchByID(branch, title));
	// 	this.dataSource.data = this.topicTree;
	// }
	// addSubTopics = (topics: string[]): void => topics.forEach(topic => this.addSubTopic(topic));

	selectNodeID(id: number): void {
		this.selectedNodeID = id;
	}

	selectNode(id: number, title: string, topicparent_id: number|null = null): void {
		this.selectedNodeID = id;
		this.selectedTitle = title;
		this.selectedParentID = topicparent_id;
	}

	getEditRequestData(id: number, description: string): void {
		this.selectedNodeID = id;
		this.selectedTitle = description;
		this.selectedParentID = null;
		this.isAnEditTopicRequest = true;
	}

	cancelEditTopicRequest(): void {
		this.isAnEditTopicRequest = false;
	}

	// API Functions
	getTopicTree(): void {
		this.API.getTopicTree().subscribe(({ data = [] }) => {
			this.addTopics(<TopicTree[]>data);
			this.search = "";
		}, err => console.error(err), () => this.loading = false);
	}

	addAPITopic({ topicTree, callback }: { topicTree: TopicTree, callback: Function }): void {
		this.loading = true;
		this.API.addTopicTree(topicTree).subscribe(() => {
			this.toastr.success("Successfully Added a Topic Tree");
			callback();
			this.getTopicTree();
		}, err => { 
			console.error(err);
			this.loading = false;
		});
	}

	addAPISubTopic({ title, callback }: { title: string, callback: Function }): void {
		this.loading = true;
		this.API.addTopicTree({
			title,
			status: 1,
			topicparent_id: this.selectedNodeID
		}).subscribe(() => {
			this.toastr.success(`Successfully Added ${title}`);
			callback();
			this.getTopicTree();
		}, err => {
			console.error(err);
			this.loading = false;
		});
	}

	editAPITopic({ topicTree, callback }: { topicTree: TopicTree, callback: Function }): void { 
		this.loading = true;
		if (this.isAnEditTopicRequest) this.API.addTopicTree({
			...topicTree,
			status: 1
		}).subscribe(() => {
			this.toastr.success(`Successfully Edited ${topicTree.title}`);
			callback();
			this.getTopicTree();
			this.removeTopicRequest(this.selectedNodeID);
			this.isAnEditTopicRequest = false;
		}, err => {
			console.error(err);
			this.loading = false;
		});
		else this.API.editTopicTree(topicTree, this.selectedNodeID).subscribe(() => {
			this.toastr.success("Successfully Edited Topic Tree");
			callback();
			this.getTopicTree();
		}, err => { 
			console.error(err);
			this.loading = false;
			this.isAnEditTopicRequest = false;
		});
	}

	deleteTopic = (callback: Function): void|ActiveToast<ToastrService> => {
		if (!this.selectedNodeID) return this.toastr.error("Something Went Wrong...");
		this.loading = true;
		this.API.deleteTopicTree(this.selectedNodeID).subscribe(res => {
			this.toastr.success(res || "Successfully Deleted Topic Tree");
			callback();
			this.getTopicTree();
		}, err => {
			console.error(err);
			this.loading = false;
		});
	}

	getTopicRequests(): void {
		this.API.getTopicRequests().subscribe(({ data = [] }) => {
			this.topicRequests = data as TopicRequest[];
		}, err => console.error(err), () => this.topicRequestLoading = false);
	}

	removeTopicRequest(id: number): void {
		this.topicRequestLoading = true;
		this.API.deleteTopicRequest(id).subscribe(res => {
			this.toastr.success(res || "Successfully Deleted Requested Topic");
			this.getTopicRequests();
		}, err => {
			console.error(err);
			this.topicRequestLoading = false;
		});
	}
}