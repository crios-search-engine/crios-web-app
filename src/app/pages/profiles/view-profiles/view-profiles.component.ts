import { Component, OnInit } from "@angular/core";
// import Profile from "../../../models/Profile.model";
import Profile from "src/app/models/Profile.model";
import { MainService } from "src/app/services/main.service";

@Component({
	selector: "app-view-profiles",
	templateUrl: "./view-profiles.component.html",
	styleUrls: ["./view-profiles.component.css"]
})

export class ViewProfilesComponent implements OnInit {

	loading: boolean = true;

	profileList: Profile[] = [];

	profileIndex: number;

	constructor(private API: MainService) { }

	ngOnInit(): void {
		this.getProfiles();
	}

	getProfiles(): void {
		this.API.getProfileByUserID().subscribe(({ data = [] }) => {
			this.profileList = <Array<Profile>>data;
      console.log(this.profileList)
		}, err => console.error(err), () => this.loading = false);
	}

	setLoading(loading: boolean): void {
		this.loading = loading;
	}
}
