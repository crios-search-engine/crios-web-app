import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FlatTreeControl } from "@angular/cdk/tree";
import { MatTreeFlatDataSource, MatTreeFlattener } from "@angular/material/tree";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ProfileTopicTreeFlatNode, ProfileTopicTreeWithCheck } from "../../../models/Profile.model";
import { MainService } from "src/app/services/main.service";
import { ActiveToast, ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";

@Component({
	selector: "app-add-profile",
	templateUrl: "./add-profile.component.html",
	styleUrls: ["./add-profile.component.css"]
})

export class AddProfileComponent implements OnInit {

	topicTree: ProfileTopicTreeWithCheck[] = [];
	initialTopicTree: ProfileTopicTreeWithCheck[] = [];
	savedTopicTree: ProfileTopicTreeWithCheck[] = [];
	flattenedTopicTree: ProfileTopicTreeWithCheck[] = [];
	hasMissingData: boolean = false;
	loading: boolean = true;
	searchInProgress: boolean;
	finishedTopicTreeIDs: Array<{ topictree_id: string }> = []; // This one will be fulled on submit

	form = new FormGroup({
		titleFormControl: new FormControl("", [Validators.required]),
		descriptionFormControl: new FormControl("", [Validators.required])
	});

	@ViewChild("closeModalBtn") closeModalBtn: ElementRef;

	constructor(
		private router: Router,
		private API: MainService,
		private toastr: ToastrService,
		private auth: AuthService
	) { }

	ngOnInit(): void {
		this.getTopicTree();
	}

	treeControl = new FlatTreeControl<ProfileTopicTreeFlatNode>(({ level }) => level, ({ expandable }) => expandable);

  	treeFlattener = new MatTreeFlattener(
		({ id, topicparent_id, subtopics, checked, title }: ProfileTopicTreeWithCheck, level: number) => ({
			expandable: subtopics?.length > 0,
			id,
			topicparent_id,
			title,
			checked,
			level
		}),
		({ level }) => level,
		({ expandable }) => expandable,
		({ subtopics }) => subtopics
	);

  	dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

	hasChild = (_: number, node: ProfileTopicTreeFlatNode) => node.expandable;
	isParent = (node: ProfileTopicTreeFlatNode) => node.level === 0;

	searchAndAddBranch = (leaf: ProfileTopicTreeWithCheck, branch: ProfileTopicTreeWithCheck): ProfileTopicTreeWithCheck => {
		if (this.searchInProgress) {
			if (Array.isArray(leaf.subtopics)) {
				if (branch.topicparent_id === leaf.id) {
					leaf.subtopics = [
						...[...leaf.subtopics || []],
						branch
					];
					this.searchInProgress = false;
				} else leaf.subtopics.forEach(leaf => this.searchAndAddBranch(leaf, branch));
			} else {
				if (branch.topicparent_id === leaf.id) {
					leaf.subtopics = [
						...[...leaf.subtopics || []],
						branch
					];
					this.searchInProgress = false;
				}
			}
		}
		return leaf;
	}
	addTopic = (branch: ProfileTopicTreeWithCheck): void => {
		if (!branch.topicparent_id) // if it's a parent branch
			this.topicTree.unshift(branch);
		else {
			this.searchInProgress = true;
			this.topicTree = this.topicTree.map(leaf => this.searchAndAddBranch(leaf, branch));
		}
	}
	addTopics = (topics: ProfileTopicTreeWithCheck[]): void => {
		if (topics.length > 0) {
			topics.forEach(topic => this.addTopic(topic));
			this.initialTopicTree = JSON.parse(JSON.stringify(this.topicTree));
		}
		this.dataSource.data = this.topicTree;
		this.searchInProgress = false;
	}

	updateTopicTreeByCheckedValue = (branch: ProfileTopicTreeWithCheck, checked: boolean, id: number): ProfileTopicTreeWithCheck => {
		if (Array.isArray(branch.subtopics)) {
			if (id === branch.id) {
				branch.checked = checked;
				// Make beneath topic trees all true since it's a parent branch
				branch = this.drillDownTopicTreeAndCheckValue(branch, checked);
			} else branch.subtopics.forEach(leaf => this.updateTopicTreeByCheckedValue(leaf, checked, id));
		} else {
			if (id === branch.id)
				branch.checked = checked;
		}
		return branch;
	}

	drillDownTopicTreeAndCheckValue(branch: ProfileTopicTreeWithCheck, checked: boolean): ProfileTopicTreeWithCheck {
		if (Array.isArray(branch.subtopics)) {
			branch.checked = checked;
			branch.subtopics.forEach(leaf => this.drillDownTopicTreeAndCheckValue(leaf, checked));
		} else branch.checked = checked;
		return branch;
	}

	updateTopicTree({ target }: Event): void {
		const { value, checked } = <HTMLInputElement>target;
		this.topicTree = this.topicTree.map(branch => this.updateTopicTreeByCheckedValue(branch, checked, parseInt(value)));
		this.updateDataSource();
	}

	updateDataSource(): void {
		this.dataSource.data = this.topicTree;
		this.expandTreeNodesByCheckedValue();
	}

	expandTreeNodesByCheckedValue(): void {
		this.treeControl.dataNodes.forEach((node, index) => {
			if (node.checked && node.expandable) this.treeControl.expand(node);
			if (node.checked) this.expandParentNodes(index, node.topicparent_id);
		});
	}

	expandParentNodes(index: number|null, parentID: number|null): void {
		if (parentID !== null) {
			for (let i = index; i > 0; i --) {
				if (this.treeControl.dataNodes[i].id === parentID) {
					this.treeControl.expand(this.treeControl.dataNodes[i]);
					if (this.treeControl.dataNodes[i].topicparent_id !== null)
						this.expandParentNodes(i, this.treeControl.dataNodes[i].topicparent_id);
				}
			}
		}
	}

	flatten = (branch: ProfileTopicTreeWithCheck): void => {
		if (Array.isArray(branch.subtopics)) {
			this.flattenedTopicTree.push(branch);
			branch.subtopics.forEach(branch => this.flatten(branch));
		} else
			this.flattenedTopicTree.push(branch);
	}

	// save only topics that were selected and return them as a topic tree (checked: true)
	// filterTopicTreeByCheckedValues(branch: ProfileTopicTreeWithCheck): boolean {
	// 	if (Array.isArray(branch.subtopics)) {
	// 		if (!branch.checked)
	// 			return false;
	// 		else branch.subtopics = branch.subtopics.filter(leaf => this.filterTopicTreeByCheckedValues(leaf));
	// 	} else {
	// 		if (!branch.checked)
	// 			return false;
	// 	}
	// 	return true;
	// }

	getIDsFromCheckedTopicTreeValues(branch: ProfileTopicTreeWithCheck): void {
		if (Array.isArray(branch.subtopics)) {
			if (branch.checked) { // If the parent is not checked, we don't have to look for children branched to add to the array
				this.finishedTopicTreeIDs.push({ topictree_id: String(branch.id) });
				branch.subtopics.forEach(branch => this.getIDsFromCheckedTopicTreeValues(branch));
			}
		} else {
			if (branch.checked)
				this.finishedTopicTreeIDs.push({ topictree_id: String(branch.id) });
		}
	}

	saveTopicTree(): void {
		this.savedTopicTree = JSON.parse(JSON.stringify(this.topicTree));
		this.flattenSavedTopicTree();
	}

	flattenSavedTopicTree(): void {
		this.flattenedTopicTree = [];
		this.savedTopicTree.forEach(branch => this.flatten(branch));
		if (this.flattenedTopicTree.length > 0 || this.flattenedTopicTree.some(({ checked }) => checked))
			this.hasMissingData = false;

    this.flattenedTopicTree = this.flattenedTopicTree.filter((v,i,a)=>a.findIndex(v2=>(v2.id===v.id))===i)
	}

	cancelTopicTree(e?: Event|undefined): void {
		if (e) e.stopPropagation();
		if (!this.savedTopicTree.length)
			this.topicTree = JSON.parse(JSON.stringify(this.initialTopicTree));
		else this.topicTree = JSON.parse(JSON.stringify(this.savedTopicTree));
		this.updateDataSource();
	}

	stopPropagation(e: Event): void {
		e.stopPropagation();
	}

	deleteTopic(id: number): void {
		this.topicTree = this.topicTree.map(branch => this.updateTopicTreeByCheckedValue(branch, false, id));
		this.saveTopicTree();
		this.updateDataSource();
	}

	// API Functions
	getTopicTree(): void {
		this.API.getTopicTree().subscribe(({ data = [] }) => {
			this.addTopics(
				(<ProfileTopicTreeWithCheck[]>data).map(obj => ({
					...obj,
					checked: false
				}))
			);
		}, err => console.error(err), () => this.loading = false);
	}

	submit(): void|boolean|ActiveToast<ToastrService> {
		if (!this.flattenedTopicTree.length || !this.flattenedTopicTree.some(({ checked }) => checked))
			return this.hasMissingData = true;

		const { titleFormControl, descriptionFormControl } = this.form.value;

		this.finishedTopicTreeIDs = [];
		this.savedTopicTree.forEach(branch => this.getIDsFromCheckedTopicTreeValues(branch));
		if (!this.finishedTopicTreeIDs.length) return this.toastr.error("Something Went Wrong...");
		console.log(this.finishedTopicTreeIDs);

		this.loading = true;
		this.API.addProfile({
			title: titleFormControl,
			description: descriptionFormControl,
			user_id: this.auth.getUserInfo()?.id,
			topictree: this.finishedTopicTreeIDs
		}).subscribe(() => {
			this.toastr.success(`Successfully Added ${titleFormControl}`);
			this.router.navigate(["/profiles"]);
		}, err => {
			console.error(err);
			this.loading = false;
		});
	}
}
