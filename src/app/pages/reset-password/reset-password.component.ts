import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
})
export class ResetPasswordComponent implements OnInit {
  signUpForm = new FormGroup({
    passwordFormControl: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    repasswordFormControl: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });

  email: string;
  token: string;
  password: any;
  equal: boolean = false;
  repassword: any;
  constructor(
    private service: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((res) => {
      console.log(res);

      this.email = res.email;
      this.token = res.token;
    });
  }

  checkMatchValidator(field1: string, field2: string) {
    this.equal = field1 == field2;
  }

  signUpRequest() {
    let json = {
      token: this.token,
      email: this.email,
      password: this.password,
      password_confirmation: this.repassword,
    };
    console.log(json);
    console.log(this.signUpForm.valid);


    if (this.signUpForm.valid)
      this.service
        .resetPassword({
          token: this.token,
          email: this.email,
          password: this.password,
          password_confirmation: this.repassword,
        })
        .then((res) => {
          localStorage.setItem('email', this.email);
          this.router.navigate(['home'])
        })
        .catch((err) => {
          console.log(err);
        });
  }
}
