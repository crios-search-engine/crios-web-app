import { Observable } from 'rxjs';
import { Component, OnDestroy, OnInit, Pipe, PipeTransform, TemplateRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { MainService } from 'src/app/services/main.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.component.html',
  styleUrls: ['./validate.component.css']
})
export class ValidateComponent implements OnInit, OnDestroy {

  loading: boolean = true;
  loadingValidate = false;
  passDocumentUrl: any = '';
  passTopicTree: any;
  checkedValidateDocs = [];
  subscription: Subscription = new Subscription();;
  visibleDocuments = [];

  constructor(
    private modalService: NgbModal,
    private sanitizer: DomSanitizer,
    private mainService: MainService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getDocuments();
  }

  ngOnDestroy(): void {
   this.subscription?.unsubscribe();
  }

  submit(){
    console.log({documents: this.checkedValidateDocs});
    if(this.checkedValidateDocs.length != 0){
      this.validateDocuments();
    }
  }

  validateDocuments() {
    this.loadingValidate = true;
    this.mainService.validateDocuments({documents: this.checkedValidateDocs}).subscribe(
      (res) => {
        this.getDocuments();
        this.loadingValidate = false;
        this.toastr.success(res);
        console.log(res);
      },
      err => {
        this.loadingValidate = false;
      },
      () => {
        this.loadingValidate = false;
      }
    );
  }

  displaySelectedConditions(event, doc_id: number) {
    if(event.target.checked) {
      let checked = this.checkedValidateDocs.find(Item => Item.document_id === doc_id);
      if(!checked){
        this.checkedValidateDocs.push({document_id: doc_id});
        console.log(this.checkedValidateDocs)
      }
    }else{
      let checked = this.checkedValidateDocs.find(Item => Item.document_id === doc_id);
      if(checked){
        this.checkedValidateDocs.forEach((element,index)=>{
          if(element.document_id == doc_id) this.checkedValidateDocs.splice(index,1);
        });
        console.log(this.checkedValidateDocs)
      }
    }
  }

  getDocuments(): any{
    this.subscription = this.mainService.getVisibleDocuments().subscribe(
      ( {data}: any ) => {
        this.visibleDocuments = data;
        console.log("visible doc", this.visibleDocuments)
        console.log("visible doc", this.visibleDocuments.length)
      },
      err => {
        console.log(err);
      },
      () => {
        this.loading = false;
      }
    );
  }

  //filter the documents to be validated
  filterPrivileges(array: any){
    this.visibleDocuments = this.visibleDocuments.filter(p => p.status === 1);
  }

  openModalViewDocument(doc_url: any){
    this.passDocumentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(doc_url);
    console.log(this.passDocumentUrl)
  }

  openModalViewTopicTree(tt: any){
    const modifiedTree = []

    tt.forEach(element => {
      console.log(element.topictree);
      modifiedTree.push(element.topictree[0])
    });

    this.passTopicTree = modifiedTree;
  }

}
