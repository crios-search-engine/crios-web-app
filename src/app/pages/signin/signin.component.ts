import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
})
export class SigninComponent implements OnInit {
  constructor(
    private service: AuthService,
    private router: Router,
    private socialAuthService: SocialAuthService
  ) {}

  form = new FormGroup({
    passwordFormControl: new FormControl('', [Validators.required]),
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
  });

  incorrectPassword: boolean = false;
  eligibleEmail: boolean = false;
  option = true;
  nonEligibleEmail: boolean = false;


  incorrectPasswordReset() {
    this.incorrectPassword = false;
  }

  submit() {
    if (this.form.valid) {
      this.service
        .checkEligibility(this.form.controls.emailFormControl.value)
        .then((res: any) => {

          if (res.registered)
            this.service
              .signIn({
                email: this.form.controls.emailFormControl.value,
                password: this.form.controls.passwordFormControl.value,
              })
              .then((res: any) => {
                localStorage.setItem(
                  'user_data',
                  JSON.stringify(res)
                );
                this.router.navigate(['/home']);
              })
              .catch(() => {
                this.incorrectPassword = true;
              });
          else if (res.eligible) this.eligibleEmail = true;
          else this.nonEligibleEmail = true;
        })
        .catch();
    }
  }

  ngOnInit() {}

  email() {
    this.option = true;
    this.eligibleEmail = false;
    this.nonEligibleEmail = false;
  }

  gmail() {
    this.option = false;
    this.eligibleEmail = false;
    this.nonEligibleEmail = false;
  }

  changeEmail() {
    this.eligibleEmail = false;
    this.nonEligibleEmail = false;
  }

  signWithGoogle(): void {
    this.socialAuthService
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((data: any) => {
        this.service.checkEligibility(data.email).then((res: any) => {
          if (res.registered) {
            this.service
            .signIn({
              email: data.email,
              password: data.id,
            })
            .then((res: any) => {
              localStorage.setItem(
                'user_data',
                JSON.stringify(res)
              );
              this.router.navigate(['/home']);
            })
            .catch(() => {
              this.incorrectPassword = true;
            });
          } else if (res.eligible) this.eligibleEmail = true;
          else this.nonEligibleEmail = true;
        });
      });
  }
}
