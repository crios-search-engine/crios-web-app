import { Subscription } from 'rxjs';
import { MainService } from './../../services/main.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {

  loading: boolean = false;
  searchItem = "";
  passedDocPath: any = "";
  RetrieveDocumentSubscription: Subscription = new Subscription();

  constructor(
    private mainService: MainService,
    private sanitizer: DomSanitizer,
  ) {}

  ngOnInit(): void {}

  checked = [];

  // documents = [
  //   { status: false, data: 'Refugees and asylum-seekers -UNHCR Lebanon' },
  //   {
  //     status: false,
  //     data: 'What is a Refugee? Definition and Meaning | USA for UNHCR',
  //   },
  //   {
  //     status: false,
  //     data: 'Refugees In Lebanon - Lebanon Refugee Camps | Anera',
  //   },
  //   {
  //     status: false,
  //     data: 'Syrian refugee crisis: Facts,FAQs, and how to help | World',
  //   },
  //   {
  //     status: false,
  //     data: 'Refugees,Asylum-seekers and Migrants | Amnesty International',
  //   },
  //   { status: false, data: 'Help For Refugees - Driven From Their Homeland' },
  //   { status: false, data: 'Humans in the Loop - Social project for refugees' },
  //   { status: false, data: 'Syrian refugee crisis' },
  // ];

  documents = [];

  resultsTitle = 'Results';

  checkedWords = [];
  checkedDocuments = [];

  words = [
    { status: false, data: 'Refugee' },
    { status: false, data: 'UNHCR' },
    { status: false, data: 'Camps' },
    { status: false, data: 'Syrian' },
    { status: false, data: 'Asylum' },
    { status: false, data: 'Homeland' },
    { status: false, data: 'Social' },
    { status: false, data: 'Crisis' },
  ];

  clearResult() {
    this.documents.forEach((element) => {
      element.status = false;
    });
  }

  clearTerms() {
    this.words.forEach((element) => {
      element.status = false;
    });
  }

  hideHeader = false;

  hide() {
    this.hideHeader = true;
  }

  relevanceFeedback() {
    // this.checked = [];
    // this.documents.forEach((element) => {
    //   if (element.status === true) this.checked.push(element.data);
    // });
    // console.log(this.checked);
    var myNewQuery = "";
    var finalQuery = "";
    this.checkedWords.forEach(w => {
      if(!myNewQuery && !this.searchItem){
        myNewQuery = myNewQuery.concat(w);
      }else {
        myNewQuery = myNewQuery.concat(" ", w);
      }
    });

    if(this.searchItem){
      finalQuery = this.searchItem.concat(myNewQuery);
    }else {
      finalQuery = myNewQuery;
    }

    var mappedString = this.checkedDocuments.map((e) => {
      return `'d${e}'`;
    })
    this.RelvenceFeedbackDocuments(mappedString.toString(), finalQuery);
  }

  updateTerms() {
    // this.checked = [];
    // this.words.forEach((element) => {
    //   if (element.status === true) this.checked.push(element.data);
    // });
    // console.log(this.checked);
    console.log(this.checkedWords);
    var myNewQuery = "";
    var finalQuery = "";
    this.checkedWords.forEach(w => {
      if(!myNewQuery && !this.searchItem){
        myNewQuery = myNewQuery.concat(w);
      }else {
        myNewQuery = myNewQuery.concat(" ", w);
      }
    });

    if(this.searchItem){
      finalQuery = this.searchItem.concat(myNewQuery);
    }else {
      finalQuery = myNewQuery;
    }
    console.log(finalQuery)

    this.RetreiveDocuments(finalQuery);
  }

  Search() {
    console.log(this.searchItem);
    //pdf simple boring
    this.RetreiveDocuments(this.searchItem);
  }

  RetreiveDocuments(queryItem: string) {
    this.loading = true;
    console.log("checked doc", this.checkedDocuments)
    this.RetrieveDocumentSubscription = this.mainService.RetreiveDocuments(queryItem)
    .subscribe(
      ({query, documents, words}: any) => {
        this.words = words;
        this.documents = documents;
        this.loading = false;
        this.checkedDocuments = [];

        console.log(this.documents)
      },
      err => {
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  RelvenceFeedbackDocuments(d: string, q: string) {
    this.loading = true;
    console.log("checked doc", this.checkedDocuments)
    this.RetrieveDocumentSubscription = this.mainService.RelvenceFeedbackDocuments(d, q)
    .subscribe(
      ({query, documents, words}: any) => {
        this.words = words;
        this.documents = documents;
        this.loading = false;
        this.checkedDocuments = [];
      },
      err => {
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  handleWordsChange(index: number) {
    if(this.checkedWords.includes(this.words[index])) {
      this.checkedWords = this.checkedWords.filter(w => w != this.words[index]);
    }else{
      this.checkedWords.push(this.words[index]);
    }
  }

  handleDocumentCheck(id: number) {
    console.log(id);
    let checkedDoc = this.checkedDocuments.find(d => d === id);
    if(!checkedDoc){
      this.checkedDocuments.push(id);
    }else{
      this.checkedDocuments = this.checkedDocuments.filter(d => d != id);
    }
    console.log(this.checkedDocuments);

  }

  openModalViewDocument(doc_url: any){
    this.passedDocPath = this.sanitizer.bypassSecurityTrustResourceUrl(doc_url);
  }

  ngOnDestroy(): void {
      this.RetrieveDocumentSubscription.unsubscribe();
  }
}
