import { EligibleEmailGuard } from './guards/eligible-email.guard';
import { TopicTreeGuard } from './guards/topic-tree.guard';
import { IndexGuard } from './guards/index.guard';
import { ValidateGuard } from './guards/validate.guard';
import { RoleGuard } from './guards/role.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { SignupComponent } from './pages/signup/signup.component';
import { SigninComponent } from './pages/signin/signin.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { ViewAccountComponent } from './pages/accounts/view-account/view-account.component';
import { EditAccountComponent } from './pages/accounts/edit-account/edit-account.component';
import { EligibleComponent } from './pages/accounts/eligible/eligible.component';
import { IndexComponent } from './pages/index/index.component';
import { ValidateComponent } from './pages/validate/validate.component';
import { ViewProfilesComponent } from './pages/profiles/view-profiles/view-profiles.component';
import { EditProfileComponent } from './pages/profiles/edit-profile/edit-profile.component';
import { AddProfileComponent } from './pages/profiles/add-profile/add-profile.component';
import { ViewRoleComponent } from './pages/roles/view-role/view-role.component';
import { AddRoleComponent } from './pages/roles/add-role/add-role.component';
import { EditRoleComponent } from './pages/roles/edit-role/edit-role.component';
import { AssignRoleComponent } from './pages/roles/assign-role/assign-role.component';
import { TopicTreeComponent } from './pages/topic-tree/topic-tree.component';
import { DocumentsComponent } from './pages/documents/documents/documents.component';
import { DocumentsAllComponent } from './pages/documents/documents-all/documents-all.component';
import { DocumentsMineComponent } from './pages/documents/documents-mine/documents-mine.component';
import { AddDocumentComponent } from './pages/documents/add-document/add-document.component';
import { EditDocumentComponent } from './pages/documents/edit-document/edit-document.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { VerifiedAccountComponent } from './pages/verified-account/verified-account.component';
import { AuthService, SignedInService } from './services/auth.service';

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent,
    canActivate: [SignedInService]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthService]
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [SignedInService],
  },
  {
    path: 'signin',
    component: SigninComponent,
    canActivate: [SignedInService],
  },
  {
    path: 'accounts/eligible',
    component: EligibleComponent,
    canActivate: [
      AuthService,
      // EligibleEmailGuard
    ],
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    canActivate: [SignedInService],
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent,
    // canActivate: [SignedInService],
  },
  {
    path: 'verified-account',
    component: VerifiedAccountComponent,
    canActivate: [AuthService],
  },
  {
    path: 'accounts/:id',
    component: ViewAccountComponent,
    canActivate: [AuthService],
  },

  {
    path: 'accounts/:id/edit',
    component: EditAccountComponent,
    canActivate: [AuthService],
  },

  {
    path: 'index',
    component: IndexComponent,
    canActivate: [
      AuthService,
      // IndexGuard
    ],
  },
  {
    path: 'validate',
    component: ValidateComponent,
    canActivate: [
      AuthService,
      // ValidateGuard
    ],
  },
  {
    path: 'profiles',
    component: ViewProfilesComponent,
    canActivate: [AuthService],
  },
  {
    path: 'profiles/add',
    component: AddProfileComponent,
    canActivate: [AuthService],
  },
  {
    path: 'profiles/:id/edit',
    component: EditProfileComponent,
    canActivate: [AuthService],
  },
  {
    path: 'roles',
    component: ViewRoleComponent,
    canActivate: [
      AuthService,
      // RoleGuard
    ],
  },
  {
    path: 'roles/add',
    component: AddRoleComponent,
    canActivate: [
      AuthService,
      // RoleGuard
    ],
  },
  {
    path: 'roles/:id/edit',
    component: EditRoleComponent,
    canActivate: [
      AuthService,
      // RoleGuard
    ],
  },
  {
    path: 'roles/assign',
    component: AssignRoleComponent,
    canActivate: [
      AuthService,
      // RoleGuard
    ],
  },
  {
    path: 'topic-tree',
    component: TopicTreeComponent,
    canActivate: [
      AuthService,
      // TopicTreeGuard
    ],
  },
  {
    path: 'documents/add',
    component: AddDocumentComponent,
    canActivate: [AuthService],
  },
  {
    path: 'documents/:id/edit',
    component: EditDocumentComponent,
    canActivate: [AuthService],
  },
  {
    path: 'documents',
    component: DocumentsComponent,
    canActivate: [AuthService],
    children: [
      {
        path: '',
        redirectTo: 'all',
        pathMatch: 'full',
      },
      {
        path: 'all',
        component: DocumentsAllComponent,
      },
      {
        path: 'mine',
        component: DocumentsMineComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
